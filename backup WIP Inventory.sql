insert into iT_Planning_CurrentInventoryValuation
select distinct ltrim(wa.wamcu) as division, 
--wa.wadoco as workorder,
-- wa.wasrst as wo_status, 
'WIP' inventory_type, --ib.ibsrp1 inventory_type,
--dba.juliantodate(wa.watrdj) as trdj,
--wa.waitm item_id,
ltrim(wa.walitm) as item_number
	,(wa.wauorg*(1+ibsrnk/10000)-wa.wasoqs)  quantity_on_hand	,0  quantity_soft_committed
	,(wa.wauorg*(1+ibsrnk/10000)-wa.wasoqs) quantity_hard_commited
	,0 quantity_on_future_commit
,  case
					when ltrim(ib.ibsrp1) IN ('RM','CP') then 0
					when ltrim(ib.ibsrp1)='FG' then 
							case 
								when (select max(sduprc) from [PRODDTA].[Cam404].[PRODDTA].[f42119] 
										where sditm = ibitm 
										and sdmcu = ibmcu 
										and sddcto IN ('SO','SA')
										and sddgl =  (select min(sddgl) 
										from [PRODDTA].[Cam404].[PRODDTA].[f42119]
                                                where sditm = ibitm 
												and sdmcu = ibmcu 
                                                and sddcto IN ('SO','SA'))) is not null
                              then  (select max(sduprc) from [PRODDTA].[Cam404].[PRODDTA].[f42119] where sditm = ibitm 
											and sdmcu = ibmcu 
                                            and sddcto IN ('SO','SA')
											and sddgl =  (select min(sddgl) from [PRODDTA].[Cam404].[PRODDTA].[f42119]
                                                                where sditm = ibitm 
																and sdmcu = ibmcu
                                                                and sddcto in ('SA','SO')))
                              else  
								case when       (select max(sduprc) from [PRODDTA].[Cam404].[PRODDTA].[f4211] where sditm = ibitm 
                                                                and sdmcu = ibmcu 
																and sddcto IN ('SO','SA')
                                                                and sdtrdj =  (select min(sdtrdj) from [PRODDTA].[Cam404].[PRODDTA].[f4211] 
                                                                                        where sditm = ibitm 
																						and sdmcu = ibmcu 
                                                                                        and sddcto in ('SA','SO'))) is not null
                                                then  (select max(sduprc) from [PRODDTA].[Cam404].[PRODDTA].[f4211] where sditm = ibitm 
                                                                  and sdmcu = ibmcu 
																  and sddcto in ('SA','SO')
                                                                  and sdtrdj =  (select min(sdtrdj) 
																						from [PRODDTA].[Cam404].[PRODDTA].[f4211] 
                                                                                          where sditm = ibitm 
																						  and sdmcu = ibmcu 
                                                                                          and sddcto in ('SA','SO')))
                                                else 0
                                          end 
							end
							--else 0
						end as SellingPriceDemandFG
, (co.councs/1000000) as standard_unit_cost 
, (co2.councs/1000000) as last_in_unit_cost
-- ibsrp3 as srp3, 
--				,Coalesce((select cast(sum((igclat-igcpat)/10000) as decimal (15,2)) from [PRODDTA].[Cam404].[PRODDTA].[f3102 ig 
--					where ig.igdoco = wa.wadoco and ig.igitm = wa.waitm and ig.igpart = 'P'),0) as wipTotal
	FROM		[PRODDTA].[Cam404].[PRODDTA].[f4801] wa join [PRODDTA].[Cam404].[PRODDTA].[f0006] mc on wa.wamcu = mc.mcmcu 
					left join [PRODDTA].[Cam404].[PRODDTA].[f3112] wl on wa.wadoco = wl.wldoco 
					and wa.wamcu = wl.wlmmcu and wl.wlopst = '30' 
					join [PRODDTA].[Cam404].[PRODDTA].[f4102] ib on wa.waitm = ib.ibitm and wa.wamcu = ib.ibmcu 
					join [PRODDTA].[Cam404].[PRODDTA].[f41021] li 
						on ib.ibmcu = li.limcu 
						and ib.ibitm = li.liitm 
					left join [PRODDTA].[Cam404].[PRODDTA].[f4105] co on ib.ibitm = co.coitm and ib.ibmcu = co.comcu and co.coledg = '07' 
				                             join [PRODDTA].[Cam404].[PRODDTA].[f4101] im on ib.ibitm = im.imitm
				                             left join [PRODDTA].[Cam404].[PRODDTA].[f4105] co2 on ib.ibitm = co2.coitm and ib.ibmcu = co2.comcu and co2.coledg = '01'
					left join (select a.wtdoco, a.wtmmcu, max(a.wtdgl) as dgl 
							from [PRODDTA].[Cam404].[PRODDTA].[f31122] a 
							join [PRODDTA].[Cam404].[PRODDTA].[f4801] b on b.wadoco = a.wtdoco 
							and b.wamcu = a.wtmmcu and b.wasrst <> '99' group by a.wtdoco, a.wtmmcu) wt2 
								on wa.wadoco = wt2.wtdoco and wa.wamcu = wt2.wtmmcu
	where ltrim(mc.mcco) not in ('00040','00050','00055','00020','00030','00035','00037','00605','00036','00038','000610','01140','00810','00610','01160','01110')
	and wa.wasrst <> '99'
	--and ltrim(wa.wadoco)='565352'
	and (ISNULL((select cast(sum((igclat-igcpat)/10000) as decimal (15,2)) from [PRODDTA].[Cam404].[PRODDTA].[f3102] ig 
					where ig.igdoco = wa.wadoco and ig.igitm = wa.waitm and ig.igpart = 'P'),0))>0;
insert into sT_Planning_CurrentInventoryValuationSum 
select division, inventory_type
, sum(quantity_on_hand*standard_unit_cost) Extended_Cost
, convert(date,getdate()) report_date
 From iT_Planning_CurrentInventoryValuation
 group by division, inventory_type;