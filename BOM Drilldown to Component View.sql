Use DataWarehouse
go
select    rtrim(ib.ibmcu) as ibmcu, 
                        ib.ibitm as ibitm, 
                        rtrim(im.imdsc1) as imdsc1, 
                        rtrim(ib.ibsrp1) as ibsrp1,
                        rtrim(ib.ibsrp2) as ibsrp2,
                        rtrim(ib.ibsrp3) as ibsrp3, 
                        rtrim(ib.ibaitm) as ibaitm, 
                        rtrim(ib.iblitm) as iblitm,
                       -- coalesce(dec(co.councs/1000000,15,7),0) as councs, 
                        rtrim(li.lilocn) as locn,
                        rtrim(li.lilotn) as lotn, 
                        coalesce(sum(li.lipqoh),0) as pqoh,
                        coalesce(sum(li.lipcom),0) as pcom,
                        coalesce(SUM(LI.LIHCOM),0) as HCOM,
                        coalesce(SUM(LI.LIFCOM),0) as FCOM,
                        SUM(LI.LIPQOH)-SUM(LI.LIPCOM)-SUM(LI.LIHCOM)-SUM(LI.LIFCOM) AS AVAIL,
                        coalesce(SUM(LI.LIQWBO),0) as QWBO,
                        --coalesce(dec(co2.councs/1000000,15,7),0) as lastIn                         case  when   (select max(sduprc) from [PRODDTA].[Cam404].[PRODDTA].[f42119] where sditm = ibitm and sdmcu = ibmcu 
                                                and sddcto = 'SO' and sddgl =  (select min(sddgl) from [PRODDTA].[Cam404].[PRODDTA].[f42119]
                                                                                                where sditm = ibitm and sdmcu = ibmcu 
                                                                                                and sddcto = 'SO')) is not null
                              then  (select max(sduprc) from [PRODDTA].[Cam404].[PRODDTA].[f42119] where sditm = ibitm and sdmcu = ibmcu 
                                                and sddcto = 'SO' and sddgl =  (select min(sddgl) from [PRODDTA].[Cam404].[PRODDTA].[f42119]
                                                                                                where sditm = ibitm and sdmcu = ibmcu
                                                                                                and sddcto = 'SO'))
                              else  case when       (select max(sduprc) from [PRODDTA].[Cam404].[PRODDTA].[f4211] where sditm = ibitm 
                                                                  and sdmcu = ibmcu and sddcto = 'SO'
                                                                  and sdtrdj =  (select min(sdtrdj) from [PRODDTA].[Cam404].[PRODDTA].[f4211]
                                                                                          where sditm = ibitm and sdmcu = ibmcu 
                                                                                          and sddcto = 'SO')) is not null
                                                then  (select max(sduprc) from [PRODDTA].[Cam404].[PRODDTA].[f4211] where sditm = ibitm 
                                                                  and sdmcu = ibmcu and sddcto = 'SO'
                                                                  and sdtrdj =  (select min(sdtrdj) from [PRODDTA].[Cam404].[PRODDTA].[f4211]
                                                                                          where sditm = ibitm and sdmcu = ibmcu 
                                                                                          and sddcto = 'SO'))
                                                else 0
                                          end
                        end as sellPrice
from              [PRODDTA].[Cam404].[PRODDTA].[f4102] ib join [PRODDTA].[Cam404].[PRODDTA].[f41021] li on ib.ibmcu = li.limcu and ib.ibitm = li.liitm 
                              left join [PRODDTA].[Cam404].[PRODDTA].[f4105] co on ib.ibitm = co.coitm and ib.ibmcu = co.comcu and co.coledg = '07' 
                              join [PRODDTA].[Cam404].[PRODDTA].[f4101] im on ib.ibitm = im.imitm
                              left join [PRODDTA].[Cam404].[PRODDTA].[f4105] co2 on ib.ibitm = co2.coitm and ib.ibmcu = co2.comcu and co2.coledg = '01'
where    1=1
--and left(rtrim(ib.ibmcu),2) = 'V4' 
--where        1=1 --     rtrim(ib.ibmcu) = '#mcu#'      
--and                     ib.ibsrp1 = '#invType#'
and               ib.ibstkt not in ('X')
group by          ib.ibmcu, ib.ibitm, ib.iblitm, ib.ibaitm, co.councs, im.imdsc1, ib.ibsrp1, ib.ibsrp2, ib.ibsrp3, co2.councs
, rtrim(li.lilocn), rtrim(li.lilotn)


--component to finished goods relationship
use DataWarehouse
go
SELECT cb.ixlitm comp_item
, cb.ixkitl fg_item
--	, ixkita third_item_num_fg
, cb.ixmmcu business_unit
, cb.ixitm parent_item_no
, cb.ixtbm bill_type fROM [PRODDTA].[Cam404].[PRODDTA].[F3002] cb 
WHERE 1=1
AND cb.IXLITM='BHW01880-8XN'
--OR IXLITM='BHW01880-8'

--item lookup
use DataWarehouse
go
--insert into sT_Sales_ItemDemand 
select  ltrim(ib.ibmcu) as division				,ltrim(ib.ibsrp1) as inv_type
                        , ltrim(ib.ibaitm) as item_number
						 ,ltrim(LI.LIPQOH)  quantity_on_hand
							,ltrim(LI.LIPCOM)  quantity_soft_committed
						,ltrim(LI.LIHCOM) quantity_hard_commited
						,ltrim(LI.LIFCOM)  quantity_on_future_commit
                    --    ,(SUM(LI.LIPQOH)-SUM(LI.LIPCOM)-SUM(LI.LIHCOM)-SUM(LI.LIFCOM)) AS Qty_Avail,
                    --    coalesce(SUM(LI.LIQWBO),0) as Qty_Workorder_Receipt						--select the max price on the earliest gl date where it is not null
				,  case when (select max(sduprc) from [PRODDTA].[Cam404].[PRODDTA].[f42119] 
										where sditm = ibitm 
										and sdmcu = ibmcu 
										and sddcto IN ('SO','SA')
										and sddgl =  (select min(sddgl) 
										from [PRODDTA].[Cam404].[PRODDTA].[f42119]
                                                where sditm = ibitm 
												and sdmcu = ibmcu 
                                                and sddcto IN ('SO','SA')) is not null
--when the max price is on the earliest gl date is not null
                              then  (select max(sduprc) from [PRODDTA].[Cam404].[PRODDTA].[f42119] where sditm = ibitm 
											and sdmcu = ibmcu 
                                            and sddcto IN ('SO','SA')
											and sddgl =  (select min(sddgl) from [PRODDTA].[Cam404].[PRODDTA].[f42119]
                                                                where sditm = ibitm 
																and sdmcu = ibmcu
                                                                and sddcto in ('SA','SO'))
--select that price is the earliest date to which the part was sold is not null
                              else  case when       (select max(sduprc) from [PRODDTA].[Cam404].[PRODDTA].[f4211] where sditm = ibitm 
                                                                and sdmcu = ibmcu 
																and sddcto IN ('SO','SA')
                                                                and sdtrdj =  (select min(sdtrdj) from [PRODDTA].[Cam404].[PRODDTA].[f4211] 
                                                                                        where sditm = ibitm 
																						and sdmcu = ibmcu 
                                                                                        and sddcto in ('SA','SO')) is not null
                                                then  (select max(sduprc) from [PRODDTA].[Cam404].[PRODDTA].[f4211] where sditm = ibitm 
                                                                  and sdmcu = ibmcu 
																  and sddcto in ('SA','SO')
                                                                  and sdtrdj =  (select min(sdtrdj) 
																						from [PRODDTA].[Cam404].[PRODDTA].[f4211] 
                                                                                          where sditm = ibitm 
																						  and sdmcu = ibmcu 
                                                                                          and sddcto in ('SA','SO')))
                                                else 0
                                          end
                        end as SellingPriceDemand						from [PRODDTA].[Cam404].[PRODDTA].[f4102] ib						join [PRODDTA].[Cam404].[PRODDTA].[f41021] li 						on ib.ibmcu = li.limcu 							and ib.ibitm = li.liitm 						where 1=1						and ib.ibstkt not in ('X')						--and ltrim(ib.ibmcu) in ('730000')						--AND ltrim(ib.ibsrp1) in ('TL','RM','CP','FG')						AND ltrim(ib.ibsrp1) in ('FG')						and ltrim(ib.ibaitm) like 'B%'						and case when (select max(sduprc) from [PRODDTA].[Cam404].[PRODDTA].[f42119] 
										where sditm = ibitm 
										and sdmcu = ibmcu 
										and sddcto in('SO','SA')
										and sddgl =  (select min(sddgl) 
										from [PRODDTA].[Cam404].[PRODDTA].[f42119]
                                                where sditm = ibitm 
												and sdmcu = ibmcu 
                                                and sddcto IN ('SO','SA'))) is not null
--when the max price is on the earliest gl date is not null
                              then  (select max(sduprc) from [PRODDTA].[Cam404].[PRODDTA].[f42119] where sditm = ibitm 
											and sdmcu = ibmcu 
                                            and sddcto IN ('SO' ,'SA')
											and sddgl =  (select min(sddgl) from [PRODDTA].[Cam404].[PRODDTA].[f42119]
                                                                where sditm = ibitm 
																and sdmcu = ibmcu
                                                                and sddcto  IN('SO','SA')))
--select that price is the earliest date to which the part was sold is not null
                              else  case when       (select max(sduprc) from [PRODDTA].[Cam404].[PRODDTA].[f4211] where sditm = ibitm 
                                                                and sdmcu = ibmcu 
																and sddcto IN ('SO','SA')
                                                                and sdtrdj =  (select min(sdtrdj) from [PRODDTA].[Cam404].[PRODDTA].[f4211] 
                                                                                        where sditm = ibitm 
																						and sdmcu = ibmcu 
                                                                                        and sddcto IN ('SO','SA'))) is not null
                                                then  (select max(sduprc) from [PRODDTA].[Cam404].[PRODDTA].[f4211] where sditm = ibitm 
                                                                  and sdmcu = ibmcu 
																  and sddcto IN ('SO','SA')
                                                                  and sdtrdj =  (select min(sdtrdj) 
																						from [PRODDTA].[Cam404].[PRODDTA].[f4211] 
                                                                                          where sditm = ibitm 
																						  and sdmcu = ibmcu 
                                                                                          and sddcto IN('SO','SA')))
                                                else 0
                                          end
                        end<>0						--and ltrim(ib.ibaitm)='BFN00340-3E6K'						--group by ltrim(ib.ibmcu) 						--	 , ltrim(ib.ibsrp1) 
						--	, ltrim(ib.ibaitm)
						ORDER BY ltrim(ib.ibaitm)