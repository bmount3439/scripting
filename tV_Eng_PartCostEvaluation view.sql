use DataWarehouse
go
--drop view tV_Eng_PartCostEvaluation
create view tV_Eng_PartCostEvaluation as
select r.*
, (select w.mfoh*e.um from iT_Planning_WorkcenterRates w 
		left join dT_Eng_ExchangeRates e on
		w.division=e.division
		where 1=1 and w.workcenter=r.business_unit and w.division=r.division) MFOH_Dollars
	--A1 Matl
	, (select max(s.mtl_cost) from tV_Eng_ItemCostComponents s
		where 1=1
		and s.division=r.division
		and s.item_number=r.ITEM_NUMBER)/isnull(r.yield,0) A1_MATL
	, (r.labor_hours*r.lab_dollars/10000/nullif(r.yield,0)) B1_Dir_Lab 
	, (r.setup_dollars*r.setup_hours/nullif(r.accounting_Cost,0)) B2_SU
	, (r.machine_overhead*r.machine_hours/1000/nullif(r.yield,0)) C1_MOH
	, ((select w.mfoh*e.um from iT_Planning_WorkcenterRates w 
		left join dT_Eng_ExchangeRates e on
		w.division=e.division
		where 1=1 and w.workcenter=r.business_unit and w.division=r.division)*r.MACHINE_HOURS/10000/nullif(r.YIELD,0)) C2_MFOH
	, (r.LABOR_OVERHEAD*r.LABOR_HOURS/10000/nullif(r.YIELD,0)) C3_LOH
	, (r.FIXED_OVERHEAD*r.LABOR_HOURS/10000/nullif(r.YIELD,0)) C4_FOH
	, (case
		when right(left(r.BUSINESS_UNIT,3),2)='64' 
		then (select op.new_cost_amount from tV_Eng_OutsideProcessingCostsByItem op
					where 1=1
					and op.item_number=(r.item_number+'*OP'+convert(varchar(100),r.operation_seq_num/100)--+'_'+convert(varchar(50),r.division)
					)
					and op.division=r.division)/r.yield 
		else 0
		end) D1_OS_Cost
		--(select r.ITEM_NUMBER + convert(varchar(100),(r.operation_seq_num/100))
from iT_Eng_RouterCostingByOperation r
WHERE 1=1
--AND r.item_number='NAS6703U8'
--AND r.division='710000'
GO

--select (r.item_number+'*OP'+convert(varchar(100),r.operation_seq_num/100)+'_'+convert(varchar(50),r.division))
--From iT_Eng_RouterCostingByOperation r
--where 1=1
--and division='710000'
--AND item_number='NAS6706H10'

--create view tV_Eng_ItemCostComponents as
select ltrim(c.division) division 
	, ltrim(c.kit_item_number) item_number  
	, (ltrim(c.kit_item_number) + '_'+convert(varchar(7),ltrim(c.division)) + ltrim(bom_type)) c_lookup
	, case	
		when c.ixum='KG' then 2.204623
		else
		case
			when c.ixum='OZ' then 1/16
			else 1
			end
		end wt_conv
	, e.UM conv
	, (case	
		when c.ixum='KG' then 2.204623
		else
		case
			when c.ixum='OZ' then 1/16
			else 1
			end
		end*c.IXQNTY) new_qnty
	, (e.um*c.IEXSMC_LB) new_qnty_alt
	, case
		when c.ixum='IN' then c.IEXSMC*c.IXQNTY
		else (e.um*c.IEXSMC_LB)*(case	
			when c.ixum='KG' then 2.204623
			else
			case
				when c.ixum='OZ' then 1/16
				else 1
				end
			end*c.IXQNTY)
		end mtl_cost
	, case
		when left(c.GL_CATEGORY,2)='RM' 
		then case
		when c.ixum='IN' then c.IEXSMC*c.IXQNTY
		else (e.um*c.IEXSMC_LB)*(case	
			when c.ixum='KG' then 2.204623
			else
			case
				when c.ixum='OZ' then 1/16
				else 1
				end
			end*c.IXQNTY)
		end
		else
			case
				when c.ixum='LB' 
				then c.IEXSMC/16
				else 
					case
						when c.ixum='EA'
						then c.iexsmc*c.IXQNTY
						else 0
					end
				end
			end comp_cost
	--, c.*
	from iT_Eng_ItemCostComponents c
		left outer join dT_Eng_ExchangeRates e on
		ltrim(c.DIVISION)=e.DIVISION
where 1=1
order by item_number
--and ltrim(c.kit_item_number)='NAS6706H10'
--and ltrim(c.division)='710000'