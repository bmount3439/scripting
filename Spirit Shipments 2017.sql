use DataWarehouse
go
select top 5 so_header.SHKCOO order_num
, 'SPIRIT AEROSYSTEMS' customer
, so_header.shco customer_num
--, so_lines.sditm item_number
, so_lines.sllitm second_item_number
, so_lines.sldsc1 description_line1
--, so_lines.sddsc2 description_line2
, so_lines.slprp1 product_class
, sum(so_lines.slsoqs) quantity_shipped
--, so_lines.sdsoqs quantity_shipped
, sum(so_lines.slaexp) amount_extended_price
, convert(date,convert(char(10),dateadd(day,convert(int,right(so_lines.SLADDJ,3)),'01/01/'+left(right(so_lines.SLADDJ,5),2))-1,101)) actual_ship_date
, case	
	when so_header.SHRKCO=730000 then 'BRISTOL'
	when so_header.SHRKCO=710000 then '3V'
	when so_header.SHRKCO=720000 then 'QRP'
	when so_header.SHRKCO=750000 then 'VOSS'
	else so_header.SHRKCO 
	end division
from [PRODDTA].[Cam404].[PRODDTA].[F42019] SO_HEADER
	join [PRODDTA].[Cam404].[PRODDTA].[f42199] SO_LINES on --history lines
	--so_header.SHKCOO=so_lines.SLKCOO
	so_header.shdoco=so_lines.sldoco
where 1=1
and so_header.shdcto='SO'
--and so_header.sdlocn='FG'
--and so_header.shco in (--'17498'
--'182630','185460','185462','166103','168728','170564','172585','180343','180862','190122','172858','143655','168727'
--,'168730','162582','162583','175238','175775','183799','185459','185461','185463','185466','185467','185468','185469','190132','141448','141449'
--,'166602','180950','166599','170565','143654','184495','187057')
and so_lines.slsoqs>0 --quantity shipped
--and so_lines.sdaddj<>0 --actual ship date
--and convert(date,convert(char(10),dateadd(day,convert(int,right(so_lines.SLADDJ,3)),'01/01/'+left(right(so_lines.SLADDJ,5),2))-1,101)) between 
group by so_header.SHKCOO 
--, 'SPIRIT AEROSYSTEMS' 
, so_header.shco 
, so_lines.sllitm 
, so_lines.sldsc1 
, so_lines.slprp1 
--, DATEADD(day, CAST(RIGHT(so_lines.SDADDJ,3) AS int) – 1, CONVERT(date,LEFT(so_lines.SDADDJ,2) + '0101', 112)) actual_ship_date_new
,convert(date,convert(char(10),dateadd(day,convert(int,right(so_lines.SLADDJ,3)),'01/01/'+left(right(so_lines.SLADDJ,5),2))-1,101))
, case	
	when so_header.SHRKCO=730000 then 'BRISTOL'
	when so_header.SHRKCO=710000 then '3V'
	when so_header.SHRKCO=720000 then 'QRP'
	when so_header.SHRKCO=750000 then 'VOSS'
	else so_header.SHRKCO 
	end 
	order by convert(date,convert(char(10),dateadd(day,convert(int,right(so_lines.sladdj,3)),'01/01/'+left(right(so_lines.sladdj,5),2))-1,101))


	--select * from [PRODDTA].[Cam404].[PRODDTA].[f42199]
	--SELECT * FROM [PRODDTA].[Cam404].[PRODDTA].[f42199]
	--select * From [PRODDTA].[Cam404].[PRODDTA].[F42019] -- header
	--where 1=1
	--and SHKCOO='115733'