use DataWarehouse
go
select   trim(ib.ibmcu) as ibmcu, 
                        ib.ibitm as ibitm, 
                        trim(im.imdsc1) as imdsc1, 
                        trim(ib.ibsrp1) as ibsrp1,
                        trim(ib.ibsrp2) as ibsrp2,
                        trim(ib.ibsrp3) as ibsrp3, 
                        trim(ib.ibaitm) as ibaitm, 
                        trim(ib.iblitm) as iblitm,
                        coalesce(dec(co.councs/1000000,15,7),0) as councs, 
                        trim(li.lilocn) as locn,
                        trim(li.lilotn) as lotn, 
                        coalesce(sum(li.lipqoh),0) as pqoh,
                        coalesce(sum(li.lipcom),0) as pcom,
                        coalesce(SUM(LI.LIHCOM),0) as HCOM,
                        coalesce(SUM(LI.LIFCOM),0) as FCOM,
                        SUM(LI.LIPQOH)-SUM(LI.LIPCOM)-SUM(LI.LIHCOM)-SUM(LI.LIFCOM) AS AVAIL,
                        coalesce(SUM(LI.LIQWBO),0) as QWBO,
                        coalesce(dec(co2.councs/1000000,15,7),0) as lastIn,
                        case when   (select max(sduprc) from [PRODDTA].[Cam404].[PRODDTA].[f42119] 
														where 1=1
														and sditm = ibitm 
														and sdmcu = ibmcu 
														and sddcto = 'SO' 
														and sddgl =  (select min(sddgl), sditm, sdmcu 
																		from [PRODDTA].[Cam404].[PRODDTA].[f42119] 
                                                                        where 1=1
                                                                        and sddcto = 'SO')) is not null
                              then  (select max(sduprc) from [PRODDTA].[Cam404].[PRODDTA].[f42119] where sditm = ibitm and sdmcu = ibmcu 
                                                and sddcto = 'SO' and sddgl =  (select min(sddgl) from [PRODDTA].[Cam404].[PRODDTA].[f42119] 
                                                                                                where sditm = ibitm and sdmcu = ibmcu
                                                                                                and sddcto = 'SO'))
                              else  case when       (select max(sduprc) from [PRODDTA].[Cam404].[PRODDTA].[f4211 where sditm = ibitm 
                                                                  and sdmcu = ibmcu and sddcto = 'SO'
                                                                  and sdtrdj =  (select min(sdtrdj) from [PRODDTA].[Cam404].[PRODDTA].[f4211] 
                                                                                          where sditm = ibitm and sdmcu = ibmcu 
                                                                                          and sddcto = 'SO')) is not null
                                                then  (select max(sduprc) from [PRODDTA].[Cam404].[PRODDTA].[f4211 where sditm = ibitm 
                                                                  and sdmcu = ibmcu and sddcto = 'SO'
                                                                  and sdtrdj =  (select min(sdtrdj) from [PRODDTA].[Cam404].[PRODDTA].[f4211] 
                                                                                          where sditm = ibitm and sdmcu = ibmcu 
                                                                                          and sddcto = 'SO'))
                                                else 0
                                          end
                        end as sellPrice
from             [PRODDTA].[Cam404].[PRODDTA].[f4102] ib join PRODDTA.f41021 li on ib.ibmcu = li.limcu and ib.ibitm = li.liitm 
                              left join [PRODDTA].[Cam404].[PRODDTA].[f4105] co on ib.ibitm = co.coitm and ib.ibmcu = co.comcu and co.coledg = '07' 
                              join [PRODDTA].[Cam404].[PRODDTA].[f4101] im on ib.ibitm = im.imitm
                              left join [PRODDTA].[Cam404].[PRODDTA].[f4105] co2 on ib.ibitm = co2.coitm and ib.ibmcu = co2.comcu and co2.coledg = '01'
where      1=1
AND    left(trim(ib.ibmcu),2) = 'V4' 
--where             trim(ib.ibmcu) = '#mcu#'      
--and                     ib.ibsrp1 = '#invType#'
and               ib.ibstkt not in ('X')
group by          ib.ibmcu, ib.ibitm, ib.iblitm, ib.ibaitm, co.councs, im.imdsc1, ib.ibsrp1, ib.ibsrp2, ib.ibsrp3, co2.councs
                        , li.lilocn, li.lilotn