use DataWarehouse
go
select --SO_HEADER.shdoco order_num
 --, 'SPIRIT AEROSYSTEMS' customer
--, so_header.SHAN8 customer_num
 so_lines.sditm item_number
--, so_lines.sllnid/1000 line_num
 --so_lines.sllitm second_item_number
--, so_lines.sldsc1 description_line1
--, so_lines.sddsc2 description_line2
--DEFINED PRODUCT FAMILY FOR 3V
--PRODUCT FAMILY FIRST
--PRODUCT LINE
--PRODUCT CLASS
 ,case	
	when so_header.SHMCU='      730000' then 'BRISTOL'
	when so_header.SHMCU='      710000' then '3V'
	when so_header.SHMCU='      720000' then 'QRP'
	when so_header.SHMCU='      750000' then 'VOSS'
	else so_header.SHMCU 
	end division
	--, so_lines.slsrp3 product_class_code
	, lc.drdl01 product_family --actually this is product class
	, code2.product_code_desc product_family2
	, code.product_code_desc product_class 
 --, so_lines.slsrp4 product_family
--, so_lines.slsoqs quantity_shipped
--, so_lines.sdsoqs quantity_shipped
--, sum(so_lines.slaexp/100) amount_extended_price
, sum(so_lines.slaexp/100) amount_extended_price
--, so_lines.slnxtr next_status
--, so_lines.sllttr last_status
--, case 
--	when convert(date,convert(char(10),dateadd(day,convert(int,right(so_lines.SLADDJ,3)),'01/01/'+left(right(so_lines.SLADDJ,5),2))-1,101))='1999-12-31' then ''
--	else convert(date,convert(char(10),dateadd(day,convert(int,right(so_lines.SLADDJ,3)),'01/01/'+left(right(so_lines.SLADDJ,5),2))-1,101))
--	end actual_ship_date
--	--, so_lines.SLADDJ actual_ship_date_val
--	, convert(date,convert(char(10),dateadd(day,convert(int,right(so_lines.slivd,3)),'01/01/'+left(right(so_lines.slivd,5),2))-1,101)) invoice_date
--	--, so_lines.slivd invoice_date_val
	--, so_header.shdcto order_type
	--, so_lines.*
from [PRODDTA].[Cam404].[PRODDTA].[F42019] SO_HEADER
	join [PRODDTA].[Cam404].[PRODDTA].[f42199] SO_LINES on --history lines
	so_header.shdoco=so_lines.sldoco
	left outer join (SELECT lookup_codes.drky product_code
	, lookup_codes.drdl01 product_code_desc
 FROM [PRODCTL].[Cam404].[PRODCTL].[F0005] lookup_codes WHERE 1=1
AND lookup_codes.DRSY='41'
AND lookup_codes.DRRT='S4') lc on
	ltrim(so_lines.slsrp4)=ltrim(lc.drky)
	left outer join (SELECT lookup_codes.drky product_code
	, lookup_codes.drdl01 product_code_desc
 FROM [PRODCTL].[Cam404].[PRODCTL].[F0005] lookup_codes WHERE 1=1
AND lookup_codes.DRSY='41'
AND lookup_codes.DRRT='S3') code on 
	ltrim(so_lines.slsrp3)=ltrim(code.product_code)
left outer join (SELECT lookup_codes.drky product_code
			, lookup_codes.drdl01 product_code_desc
		 FROM [PRODCTL].[Cam404].[PRODCTL].[F0005] lookup_codes WHERE 1=1
		AND lookup_codes.DRSY='41'
		AND lookup_codes.DRRT='S2') code2 on 
		ltrim(so_lines.slsrp2)=ltrim(code2.product_code)
where 1=1
and RTRIM(so_header.SHAN8) in ('175775','175238','182630','185460','185462','166103','168728','170564','172585','180343','180862','190122','172858','143655','168727','168730','162582','162583','175238','175775','183799','185459','185461','185463','185466','185467','185468','185469','190132','141448','141449'
,'166602','180950','166599','170565','143654','184495','187057')
and so_lines.slsoqs>0 --quantity shipped--AND lc.DRSY='41'
--AND lc.DRRT='S3'--for product family--and lc.DRRT='S4'--and convert(date,convert(char(10),dateadd(day,convert(int,right(so_lines.SLADDJ,3)),'01/01/'+left(right(so_lines.SLADDJ,5),2))-1,101))<>'1999-12-31'--and convert(date,convert(char(10),dateadd(day,convert(int,right(so_lines.SLADDJ,3)),'01/01/'+left(right(so_lines.SLADDJ,5),2))-1,101)) between--'2017-01-01' and '2017-12-31'and so_lines.sladdj like '117%'and so_lines.slivd like '117%' --invoice date		and so_header.SHMCU in ('      730000','      710000','      720000','      750000')		and so_lines.slqtyt>0 --units shipped to date		--added		--and SO_HEADER.shdoco='1645005'		--and (so_lines.sllnid/1000)=20.5		group by --so_lines.slsrp4
		lc.drdl01
		 --so_lines.slsrp3
		 --so_lines.slsrp4
		 --, so_lines.sllitm
		, case	
	when so_header.SHMCU='      730000' then 'BRISTOL'
	when so_header.SHMCU='      710000' then '3V'
	when so_header.SHMCU='      720000' then 'QRP'
	when so_header.SHMCU='      750000' then 'VOSS'
	else so_header.SHMCU 
	end 	--order by so_header.shdoco