USE [MoellerERP]
GO

--create view tV_Inventory_FG as
select oj.jonum Job, convert(date,oj.issued) IssuedDate, replace(concat(oj.jonum,convert(date,oj.issued)),'-','') NewJobID, oj.pn
, oj.reqwt14 ReqWeight, oj.finwt14 FinalWeight, oj.hours14 TotalHours --, oj.totcost14 TotalCost
, oj.REQWT14
--, ((convert(decimal,oj.REQWT14)/1000)*convert(decimal,oj.MATLCOST))*rh.Quantity MatCost
--, oj.MATLCOST MaterialCost
, oj.matcostper MatCostPer
, oj.killdate KillDate --, oj.*
----, (rh.operation_cost_per_ea) TotalCost_By_IssueDate
--, sum(rh.operation_cost_per_ea) Total_Op_Cost_By_IssueDate
--, ((convert(decimal,oj.REQWT14)/1000)*convert(decimal,oj.MATLCOST))*rh.Quantity+sum(rh.operation_cost_per_ea) TotalCost_By_IssueDate
----add material cost per lb to toatl cost by issue date
, oj.QT Quantity
From iT_JOBHIST oj 
left outer join sT_Routing_OperationRouterHistory rh
on oj.jonum=rh.job
and replace(concat(oj.jonum,convert(date,oj.issued)),'-','')=rh.NewJobID
and rh.Quantity=oj.QT
where 1=1 
--and jonum='10097'
and ltrim(oj.jonum)<>''
and convert(int,ltrim(oj.jonum))>0
--and Issue_Date='2018-05-25'
and ltrim(oj.KILLDATE)<>''
group by oj.jonum, convert(date,oj.issued), replace(concat(oj.jonum,convert(date,oj.issued)),'-',''), oj.pn
, oj.reqwt14, oj.finwt14, oj.hours14 --, oj.totcost14 TotalCost
, oj.matcostper, oj.killdate,oj.REQWT14, oj.MATLCOST, oj.QT
GO
