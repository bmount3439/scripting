select SO_HEADER.shdoco,
 so_lines.sllitm item_number
,case	
	when so_header.SHMCU='      730000' then 'BRISTOL'
	when so_header.SHMCU='      710000' then '3V'
	when so_header.SHMCU='      720000' then 'QRP'
	when so_header.SHMCU='      750000' then 'VOSS'
	else so_header.SHMCU 
	end division
	, CODE1.product_code
	, code1.product_code_desc product_family 
, code2.product_code
	, code2.product_code_desc product_family2
, code.product_code
	, code.product_code_desc product_class 
-- , sum(so_lines.slaexp/100) amount_extended_price
from  [PRODDTA].[Cam404].[PRODDTA].[F42019] SO_HEADER
	join  [PRODDTA].[Cam404].[PRODDTA].[f42199] SO_LINES on --history lines
	so_header.shdoco=so_lines.sldoco
	left outer join (SELECT lookup_codes.drky product_code
	, lookup_codes.drdl01 product_code_desc
 FROM [PRODCTL].[Cam404].[PRODCTL].[F0005] lookup_codes WHERE 1=1
and lookup_codes.drky <>'          '
AND lookup_codes.DRSY='41'
AND lookup_codes.DRRT='S4') code1 on
	ltrim(so_lines.slsrp4)=ltrim(code1.product_code)
	left outer join (SELECT lookup_codes.drky product_code
	, lookup_codes.drdl01 product_code_desc
 FROM [PRODCTL].[Cam404].[PRODCTL].[F0005] lookup_codes WHERE 1=1
and lookup_codes.drky <>'          '
AND lookup_codes.DRSY='41'
AND lookup_codes.DRRT='S3') code on 
	ltrim(so_lines.slsrp3)=ltrim(code.product_code)
left outer join (SELECT lookup_codes.drky product_code
			, lookup_codes.drdl01 product_code_desc
		 FROM [PRODCTL].[Cam404].[PRODCTL].[F0005] lookup_codes WHERE 1=1
		AND lookup_codes.DRSY='41'
		AND lookup_codes.DRRT='S2'
and lookup_codes.drky <>'          '
) code2 on 
		ltrim(so_lines.slsrp2)=ltrim(code2.product_code)
where 1=1
	and ltrim(so_lines.sllitm) is not null
		and so_header.SHMCU in ('      750000','      730000','      710000','      720000')
		group by SO_HEADER.shdoco, 
so_lines.sllitm
		, code1.product_code
	, code1.product_code_desc 
, code2.product_code
	, code2.product_code_desc 
, code.product_code
	, code.product_code_desc 
		, case	
	when so_header.SHMCU='      730000' then 'BRISTOL'
	when so_header.SHMCU='      710000' then '3V'
	when so_header.SHMCU='      720000' then 'QRP'
	when so_header.SHMCU='      750000' then 'VOSS'
	else so_header.SHMCU 
	end 