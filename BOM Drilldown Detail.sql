USE [DataWarehouse]
GO


--drop view tV_Eng_BOMDrilldown
create view [dbo].[tV_Eng_BOMDrilldown] as
select * From (select distinct f.division, f.FG_ITEM, f.comp_item, c.comp_unit_standard_cost   --, c_2.comp_item level_2_comp, c_3.comp_item level_3_comp
	From iT_Eng_FinishedGoodsCompRelationship  f
	 left join iT_Eng_FinishedGoodsCompRelationship c on
	f.comp_item=c.FG_ITEM
	and f.division=c.division
	where 1=1
	--and f.FG_ITEM='BGC01804-3-14-3-5'
union
select distinct f.division, f.fg_item ,c.comp_item,c.comp_unit_standard_cost  --, c_2.comp_item level_2_comp--, c_3.comp_item level_3_comp
	From iT_Eng_FinishedGoodsCompRelationship  f
	 left join iT_Eng_FinishedGoodsCompRelationship c on
	f.comp_item=c.FG_ITEM
	and f.division=c.division
	where 1=1
	--and f.FG_ITEM='BGC01804-3-14-3-5'
	--and c.comp_item='BCH463N14NF'
union
select distinct f.division, f.FG_ITEM, c_2.comp_item,c_2.comp_unit_standard_cost  --, c_3.comp_item level_3_comp
	From iT_Eng_FinishedGoodsCompRelationship  f
	 left join iT_Eng_FinishedGoodsCompRelationship c on
	f.comp_item=c.FG_ITEM
	and f.division=c.division
	left join iT_Eng_FinishedGoodsCompRelationship c_2 on
	c.comp_item=c_2.FG_ITEM
	and c.division=c_2.division
	where 1=1
	--and f.FG_ITEM='BGC01804-3-14-3-5'
union
select distinct f.division, f.FG_ITEM, c_3.comp_item,c_3.comp_unit_standard_cost 
	From iT_Eng_FinishedGoodsCompRelationship  f
	 left join iT_Eng_FinishedGoodsCompRelationship c on
	f.comp_item=c.FG_ITEM
	and f.division=c.division
	left join iT_Eng_FinishedGoodsCompRelationship c_2 on
	c.comp_item=c_2.FG_ITEM
	and c.division=c_2.division
	left join iT_Eng_FinishedGoodsCompRelationship c_3 on
	c_2.comp_item=c_3.FG_ITEM
	and c_2.division=c_3.division
	where 1=1
	and f.FG_ITEM='BGC01804-3-14-3-5'
	) bom
where 1=1 --and bom.comp_item='BCH00325-3-010-2U'
--AND bom.FG_ITEM='BGC01804-3-14-3-5'
--and bom.comp_item='BCN00020A3-3'
GO


