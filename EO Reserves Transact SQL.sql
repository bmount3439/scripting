Use SharePoint_test
go
--select * from sT_Inventory_Reserves
--alter table sT_Inventory_Reserves drop column discrete_demand
truncate table sT_Inventory_Reserves;
insert into sT_Inventory_Reserves
select ltrim(limcu) as mcu, 
                              ltrim(iblitm) as litm, 
                                  (ibglpt) as glpt,  
                              isnull(round(sum(li.lipqoh)*convert(decimal,(co.councs/1000000)),2),0) extended_cost, 
                              (36 * convert(decimal,(
                                      (select top 1 sum(lta2.amu) 
                              from [PRODDTA].[CAM404].[COLDFUSE].[TVLTA] lta2 
                              where lta2.itm = ib.ibitm and(lta2.mcu) = li.limcu 
                              and lta2.an8 = 140991 
                              and convert(date,lta2.rptDate) between(select convert(date,c.[date]) min_date 
                              from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] c 
                              join (select min(cal.calendarsmrt_key) 
                               calendarsmrt_key 
                               from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal 
                               join (select year, month From [PRODDTA].[Cam404].[zpemjde].[calendar_dim] 
                               cal2 
                              where 1 = 1 
                              and(DateAdd(yy,-1,convert(Date,getdate()))) = cal2.[date]) cal_sub 
                              on cal.year = cal_sub.year 
                              and cal.month = cal_sub.month 
                              where 1 = 1 
                              and cal.number_working_days > 0) cal on 
                               c.calendarsmrt_key = cal.calendarsmrt_key) and(select distinct convert(date,getdate()) from [DataWarehouse].[dbo].[Calendar_dim]) 
                              group by  lta2.rptDate , lta2.itm, lta2.mcu 
                              --order by lta2.rptdate desc 
                               ))+((select top 1 sum(lta3.amu) 
                              from [PRODDTA].[CAM404].[COLDFUSE].[TVLTA] lta3 
                              where lta3.itm = ib.ibitm and(lta3.mcu) = li.limcu 
                              and lta3.an8 = 141448 
                              and convert(Date,lta3.rptDate) between(select convert(date,c.[date]) min_date 
                              from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] c 
                              join (select min(cal.calendarsmrt_key) 
                               calendarsmrt_key 
                               from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal 
                               join (select year, month From [PRODDTA].[Cam404].[zpemjde].[calendar_dim] 
                               cal2 
                              where 1 = 1 
                              and(DateAdd(yy,-1,convert(Date,getdate()))) = cal2.[date]) cal_sub 
                              on cal.year = cal_sub.year 
                              and cal.month = cal_sub.month 
                              where 1 = 1 
                              and cal.number_working_days > 0) cal on 
                               c.calendarsmrt_key = cal.calendarsmrt_key) and(select distinct convert(date,getdate()) from [DataWarehouse].[dbo].[Calendar_dim] ) 
                              group by  lta3.rptDate , lta3.itm, lta3.mcu 
                              --order by lta3.rptdate desc 
                               )+isnull((select top 1 sum(lta4.amu) 
                              from [PRODDTA].[CAM404].[COLDFUSE].[TVLTA] lta4 
                              where lta4.itm = ib.ibitm and(lta4.mcu) = li.limcu 
                              and lta4.an8 = 142097 
                              and converT(date,lta4.rptDate) between(select convert(date,c.[date]) min_date 
                              from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] c 
                              join (select min(cal.calendarsmrt_key) 
                               calendarsmrt_key 
                               from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal 
                               join (select [year], [month] From [PRODDTA].[Cam404].[zpemjde].[calendar_dim] 
                               cal2 
                              where 1 = 1 
                              and DateAdd(yy,-1,convert(Date,getdate())) = cal2.[date]) cal_sub 
                              on cal.year = cal_sub.year 
                              and cal.month = cal_sub.month 
                              where 1 = 1 
                              and cal.number_working_days > 0) cal on 
                               c.calendarsmrt_key = cal.calendarsmrt_key) and(select distinct convert(date,getdate()) from [DataWarehouse].[dbo].[Calendar_dim]) 
                              group by  lta4.rptDate , lta4.itm, lta4.mcu 
                              ),0)))) minMaxDemand, 
                                              sum(li.lipqoh) as pqoh,  
                                             im.imuom1 
							 , Convert(decimal,(case 
                                 when convert(decimal,sum(li.lipqoh)) > ((case 
                                when(isnull(case  when left(ibglpt, 2) = 'FG'
                                     then(select sum(sd2.sdsoqs) from [PRODDTA].[Cam404].[PRODDTA].[f42119] sd2                                                                                             
                                        left outer join [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal2 on sd2.sddgl = cal2.juliandate                                                                     
                                          where sd2.sditm = ib.ibitm and(sd2.sdmcu) = li.limcu                                                                                       
                                     and cal2.[date] between(select convert(date,c.[date]) min_date                                                                                              
                             from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] c                                                                                       
                             join (select min(cal.calendarsmrt_key)                                                                            
                              calendarsmrt_key                                                                                                 
                              from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal                                                                                    
                             join (select [year], [month] From [PRODDTA].[Cam404].[zpemjde].[calendar_dim]                                                                
                              cal2    
                             where 1 = 1  
                             and((select DateAdd(yy,-1,DateAdd(MM,-3,convert(Date,getdate()))))) = cal2.[date]) cal_sub                                                      
                             on cal.[year] = cal_sub.[year]
                             and cal.[month] = cal_sub.[month]
                             where 1 = 1 
                             and cal.number_working_days > 0) cal on 
                              c.calendarsmrt_key = cal.calendarsmrt_key) and (select distinct convert(date,getdate()) from [DataWarehouse].[dbo].[Calendar_dim]))          
                                 else (select (case 
													when iltrum = im.imuom1 then iltrqt                                                    
													else iltrqt / (select 
																		convert(decimal,(
																		ucconv / 10000000)) 
																		from [PRODDTA].[Cam404].[PRODDTA].[f41003]                                       
																		where ucum = im.imuom1 and ucrum = iltrum)                                                            
																		end )*(-1)
												from [PRODDTA].[Cam404].[PRODDTA].[f4111]                                                                                            
												left outer join [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal on iltrdj = cal.juliandate                                                      
												where ilitm = ib.ibitm                                                                                       
												and(ilmcu) = li.limcu and ildct = 'IM'                                                                        
												and cal.[date] between(select convert(date,c.[date]) min_date                                                             
                             from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] c                                                                                       
                             join (select min(cal.calendarsmrt_key)                                                                            
                              calendarsmrt_key                                                                                                 
                              from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal                                                                                    
                             join (select [year], [month] From [PRODDTA].[Cam404].[zpemjde].[calendar_dim]                                                                
                              cal2                                                                                                             
                             where 1 = 1                                                                                                       
                             and((select DateAdd(yy,-1,DateAdd(MM,-3,convert(Date,getdate())))))= cal2.[date]) cal_sub                                                       
                               on cal.[year] = cal_sub.[year]
                             and cal.[month] = cal_sub.[month]
                             where 1 = 1                                                                                                       
                             and cal.number_working_days > 0) cal on                                                                           
                              c.calendarsmrt_key = cal.calendarsmrt_key) and(select distinct convert(date,getdate()) from [DataWarehouse].[dbo].[Calendar_dim]))          
                             end,0))/ (isnull((DateDiff(dd,(convert(date,getdate())),isnull((((select min(cal.[date]) from [PRODDTA].[Cam404].[PRODDTA].[f4108] 
								left outer join [DataWarehouse].[dbo].[Calendar_dim] cal
								on ioohdj=cal.juliandate
                                     where (iomcu) = li.limcu and ioitm = ibitm))),                                                                                                
                                     (((select min(iltrdj) from [PRODDTA].[Cam404].[PRODDTA].[f4111] 
									 left outer join [DataWarehouse].[dbo].[Calendar_dim] cal on iltrdj=cal.juliandate
									 where (ilmcu) = li.limcu                                                         
                                     and ibitm = ilitm))))) / 30),1))*36                                                                                                            
                                       >                                                                                                                                           
                                       (isnull((select sum(sduorg) from [PRODDTA].[Cam404].[PRODDTA].[f4211] sd where (sd.sdmcu) = li.limcu and sd.sditm = ib.ibitm                                     
                                      and sd.sddcto not in ('SA', 'ST', 'SQ') and sd.sdnxtr <> '999'), 0) +                                                                        
                                       isnull((select sum(lipcom) from [PRODDTA].[Cam404].[PRODDTA].[f41021] where (limcu) = li.limcu and left(ltrim(liglpt), 2) in ('CP', 'RM') and liitm = ibitm),0))  
                                 then                                                                                                                                              
                                      (isnull(case  when left(ibglpt, 2) = 'FG'                                                                                                  
                                     then(select sum(sd2.sdsoqs) from [PRODDTA].[Cam404].[PRODDTA].[f42119] sd2                                                                                           
                                        left outer join [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal2 on sd2.sddgl = cal2.juliandate                                                                   
                                          where sd2.sditm = ib.ibitm and(sd2.sdmcu) = li.limcu                                                                                     
                                     and cal2.[date] between(select convert(date,c.[date]) min_date                                                                                            
                             from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] c                                                                                                                           
                             join (select min(cal.calendarsmrt_key)                                                                                                                
                              calendarsmrt_key                                                                                                                                     
                              from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal                                                                                                                        
                             join (select [year], [month] From [PRODDTA].[Cam404].[zpemjde].[calendar_dim]                                                                                                    
                              cal2                                                                                                                                                 
                             where 1 = 1                                                                                                                                           
                             and((select DateAdd(yy,-1,DateAdd(MM,-3,convert(Date,getdate()))))) = cal2.[date]) cal_sub                                                                                          
                             on cal.[year] = cal_sub.[year]                                                                                                                            
                             and cal.[month] = cal_sub.[month]                                              
                             where 1 = 1                                                                                                                                           
                             and cal.number_working_days > 0) cal on                                                                                                               
                              c.calendarsmrt_key = cal.calendarsmrt_key) and(select distinct convert(date,getdate()) from [DataWarehouse].[dbo].[Calendar_dim]))                                              
                                 else (select (case  when iltrum = im.imuom1 then iltrqt                                                                                        
                                 else iltrqt / (select convert(decimal,(ucconv / 10000000)) from [PRODDTA].[Cam404].[PRODDTA].[f41003]                                                                           
                                         where ucum = im.imuom1 and ucrum = iltrum)                                                                                                
                                         end )*(-1)                                                                                                                                
                                 from [PRODDTA].[Cam404].[PRODDTA].[f4111]                                                                                                                                
                                 left outer                                                                                                                                        
                                 join [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal on iltrdj = cal.juliandate                                                                                          
                                  where ilitm = ib.ibitm                                                                                                                           
                                 and(ilmcu) = li.limcu and ildct = 'IM'                                                                                                            
                                 and cal.[date] between(select convert(date,c.[date]) min_date                                                                                                 
                             from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] c                                                                                                                           
                             join (select min(cal.calendarsmrt_key)                                                                                                                
                              calendarsmrt_key                                                                                                                                     
                              from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal                                                                                                                        
                             join (select [year], [month] From [PRODDTA].[Cam404].[zpemjde].[calendar_dim]                                                                                                    
                              cal2                                                                                                                                                 
                             where 1 = 1                                                                                                                                           
                             and((select DateAdd(yy,-1,DateAdd(MM,-3,convert(Date,getdate())))))= cal2.[date]) cal_sub                                                                                           
                               on cal.[year] = cal_sub.[year]                                                                                                                          
                             and cal.[month] = cal_sub.[month]                                                                                                                         
                             where 1 = 1                                                                                                                                           
                             and cal.number_working_days > 0) cal on                                                                                                               
                              c.calendarsmrt_key = cal.calendarsmrt_key) and(select distinct convert(date,getdate()) from [DataWarehouse].[dbo].[Calendar_dim]))                                              
                             end,0))/ 
							 --DateDiff(day, isnull((days(convert(date,getdate()),isnull(days(dba.juliantodate((select min(ioohdj) from [PRODDTA].[Cam404].[PRODDTA].[f4108]                                          
        --                             where (iomcu) = li.limcu and ioitm = ibitm))),                                                                                                
        --                             days(dba.juliantodate((select min(iltrdj) from [PRODDTA].[Cam404].[PRODDTA].[f4111] where (ilmcu) = li.limcu                                                         
        --                             and ibitm = ilitm)))) / 30),1))/ 15 * 36       
		(isnull((DateDiff(dd,(convert(date,getdate())),isnull((((select min(cal.[date]) from [PRODDTA].[Cam404].[PRODDTA].[f4108] 
								left outer join [DataWarehouse].[dbo].[Calendar_dim] cal
								on ioohdj=cal.juliandate
                                     where (iomcu) = li.limcu and ioitm = ibitm))),                                                                                                
                                     (((select min(iltrdj) from [PRODDTA].[Cam404].[PRODDTA].[f4111] 
									 left outer join [DataWarehouse].[dbo].[Calendar_dim] cal on iltrdj=cal.juliandate
									 where (ilmcu) = li.limcu 
                                     and ibitm = ilitm))))) / 30),1))/15 * 36 
                                 else 
                                     (isnull((select sum(sduorg) from [PRODDTA].[Cam404].[PRODDTA].[f4211] sd where (sd.sdmcu) = li.limcu and sd.sditm = ib.ibitm                                       
                                     and sd.sddcto not in ('SA', 'ST', 'SQ') and sd.sdnxtr <> '999'), 0) +                                                                         
                                     isnull((select sum(lipcom) from [PRODDTA].[Cam404].[PRODDTA].[f41021] where (limcu) = li.limcu and left(ltrim(liglpt), 2) in ('CP', 'RM') and liitm = ibitm),0))    
                                 end)+(36 * convert(decimal,(isnull(                                                                                                                          
                                     (select top 1 sum(lta2.amu)                                                                                                                        
                                 from [PRODDTA].[CAM404].[COLDFUSE].[TVLTA] lta2                                                                                                                          
                                 where lta2.itm = ib.ibitm and(lta2.mcu) = li.limcu                                                                                                
                                 and lta2.an8 = 140991                                                                                                                             
                                 and convert(date,lta2.rptDate) between(select convert(date,c.[date]) min_date                                                                                       
                             from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] c                                                                                                                           
                             join (select min(cal.calendarsmrt_key)                                                                                                                
                              calendarsmrt_key                                                                                                                                     
                              from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal                                                                                                                        
                             join (select [year], [month] From [PRODDTA].[Cam404].[zpemjde].[calendar_dim]                                                                                                    
                              cal2                                                                                                                                                 
                             where 1 = 1                                                                                                                                           
                             and(DateAdd(yy,-1,convert(Date,getdate()))) = cal2.[date]) cal_sub                                                                                                       
                             on cal.[year] = cal_sub.[year]
                             and cal.[month] = cal_sub.[month]                                                                                                    
                             where 1 = 1                                                                                                                      
                             and cal.number_working_days > 0) cal on                                                                                          
                              c.calendarsmrt_key = cal.calendarsmrt_key) and(select distinct convert(date,getdate()) from [DataWarehouse].[dbo].[Calendar_dim])                          
                                group by  lta2.rptDate , lta2.itm, lta2.mcu                                                                                   
                                 --order by lta2.rptdate desc                                                                                                   
                                  ),0) 	+isnull((select top 1 sum(lta3.amu)                                                               
                                 from [PRODDTA].[CAM404].[COLDFUSE].[TVLTA] lta3                                                                                                     
                                 where lta3.itm = ib.ibitm and(lta3.mcu) = li.limcu                                                                           
                                 and lta3.an8 = 141448 
                                 and convert(Date,lta3.rptDate) between(select convert(Date,c.[date]) min_date                                                                  
                             from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] c                                                                                                      
                             join (select min(cal.calendarsmrt_key)                                                                                           
                              calendarsmrt_key                                                                                                                
                              from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal                                                                                                   
                             join (select year, month From [PRODDTA].[Cam404].[zpemjde].[calendar_dim]                                                                               
                              cal2                                                                                                                            
                             where 1 = 1                                                                                                                      
                             and(DateAdd(yy,-1,convert(Date,getdate()))) = cal2.[date]) cal_sub                                                                                  
                             on cal.year = cal_sub.year                                                                                                       
                             and cal.month = cal_sub.month                                                                                                    
                             where 1 = 1                                                                                                                      
                             and cal.number_working_days > 0) cal on                                                                                          
                              c.calendarsmrt_key = cal.calendarsmrt_key) and(select distinct convert(date,getdate()) from [DataWarehouse].[dbo].[Calendar_dim])                          
                                 group by  lta3.rptDate , lta3.itm, lta3.mcu                                                                                  
                                 --order by lta3.rptdate desc                                                                                                   
                                 ),0) +isnull((select top 1 sum(lta4.amu)                                                                   
                                 from [PRODDTA].[CAM404].[COLDFUSE].[TVLTA] lta4                                                                                                     
                                 where lta4.itm = ib.ibitm and(lta4.mcu) = li.limcu                                                                           
                                 and lta4.an8 = 142097                                                                                                        
                                 and convert(date,lta4.rptDate) between(select convert(date,c.[date]) min_date                                                                  
                             from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] c                                                                                                      
                             join (select min(cal.calendarsmrt_key)                                                                                           
                              calendarsmrt_key                                                                                                                
                              from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal                                                                                                   
                             join (select [year], [month] From [PRODDTA].[Cam404].[zpemjde].[calendar_dim]                                                                               
                              cal2                                                                                                                            
                             where 1 = 1                                                                                                                      
                             and(DateAdd(yy,-1,convert(Date,getdate()))) = cal2.[date]) cal_sub                                                                                  
                             on cal.year = cal_sub.year                                                                                                       
                             and cal.month = cal_sub.month                                                                                                    
                             where 1 = 1                                                                                                                      
                             and cal.number_working_days > 0) cal on                                                                                          
                              c.calendarsmrt_key = cal.calendarsmrt_key) and(select distinct convert(date,getdate()) from [DataWarehouse].[dbo].[Calendar_dim])                          
                                 group by  lta4.rptDate , lta4.itm, lta4.mcu                                                                                  
                                 --order by lta4.rptdate desc                                                                                                   
                                  ),0))))     )                                                                                            
                                 then(convert(Decimal,(sum(li.lipqoh))) - (case                                                                                             
                                 when(isnull(case  when left(ibglpt, 2) = 'FG'                                                                              
                                     then(select sum(sd2.sdsoqs) from [PRODDTA].[Cam404].[PRODDTA].[f42119] sd2                                                                      
                                        left outer join [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal2 on sd2.sddgl = cal2.juliandate                                              
                                          where sd2.sditm = ib.ibitm and(sd2.sdmcu) = li.limcu                                                                
                                     and cal2.[date] between(select convert(date,c.[date]) min_date                                                                       
                             from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] c                                                                                                      
                             join (select min(cal.calendarsmrt_key)                                                                                           
                              calendarsmrt_key                                                                                                                
                              from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal                                                                                                   
                             join (select year, month From [PRODDTA].[Cam404].[zpemjde].[calendar_dim]                                                                               
                              cal2                                                                                                                            
                             where 1 = 1                                                                                                                      
                             and((select DateAdd(yy,-1,DateAdd(MM,-3,convert(Date,getdate()))))) = cal2.[date]) cal_sub                                                                     
                             on cal.year = cal_sub.year                                                                                                       
                             and cal.month = cal_sub.month                                                                                                    
                             where 1 = 1                                                                                                                      
                             and cal.number_working_days > 0) cal on                                                                                          
                              c.calendarsmrt_key = cal.calendarsmrt_key) and(select distinct convert(date,getdate()) from [DataWarehouse].[dbo].[Calendar_dim]))                         
                                 else (select (case  when iltrum = im.imuom1 then iltrqt                                                                   
                                 else iltrqt / (select convert(decimal,(ucconv / 10000000)) from [PRODDTA].[Cam404].[PRODDTA].[f41003]                                                      
                                         where ucum = im.imuom1 and ucrum = iltrum)                                                                           
                                         end )*(-1)                                                                                                           
                                 from [PRODDTA].[Cam404].[PRODDTA].[f4111]                                                                                                           
                                 left outer                                                                                                                                        
                                 join [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal on iltrdj = cal.juliandate                                                                                          
                                  where ilitm = ib.ibitm                                                                                                                           
                                 and(ilmcu) = li.limcu and ildct = 'IM'                                                                                                            
                                and cal.[date] between(select convert(Date,c.[date]) min_date                                                                                                  
                             from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] c                                                                                                                           
                             join (select min(cal.calendarsmrt_key) 
                              calendarsmrt_key
                              from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal                                                                                                                        
                             join (select year, month From [PRODDTA].[Cam404].[zpemjde].[calendar_dim]                                                                                                    
                              cal2                                                                                                                                                 
                             where 1 = 1                                                                                                                                           
                             and((select DateAdd(yy,-1,DateAdd(MM,-3,convert(Date,getdate())))))= cal2.[date]) cal_sub                                                                                           
                               on cal.year = cal_sub.year                                                                                                                          
                             and cal.month = cal_sub.month  
                             where 1 = 1 
                             and cal.number_working_days > 0) cal on                                                                                                               
                              c.calendarsmrt_key = cal.calendarsmrt_key) and(select distinct convert(date,getdate()) from [DataWarehouse].[dbo].[Calendar_dim]))                                              
                             end,0))/ 
							 --(DateDiff(days,isnull(((convert(date,getdate())),isnull((dba.juliantodate((select min(ioohdj) from [PRODDTA].[Cam404].[PRODDTA].[f4108]                                          
        --                             where (iomcu) = li.limcu and ioitm = ibitm))),                                                                                                
        --                             days(dba.juliantodate((select min(iltrdj) from [PRODDTA].[Cam404].[PRODDTA].[f4111] where (ilmcu) = li.limcu                                                         
        --                             and ibitm = ilitm)))) / 30),1))*36    
		(isnull((DateDiff(dd,(convert(date,getdate())),isnull((((select min(cal.[date]) from [PRODDTA].[Cam404].[PRODDTA].[f4108] 
								left outer join [DataWarehouse].[dbo].[Calendar_dim] cal
								on ioohdj=cal.juliandate
                                     where (iomcu) = li.limcu and ioitm = ibitm))),                                                                                                
                                     (((select min(iltrdj) from [PRODDTA].[Cam404].[PRODDTA].[f4111] 
									 left outer join [DataWarehouse].[dbo].[Calendar_dim] cal on iltrdj=cal.juliandate
									 where (ilmcu) = li.limcu                                                         
                                     and ibitm = ilitm))))) / 30),1))*36                                                                                                          
                                       >                                                                                                                                           
                                       (isnull((select sum(sduorg) from [PRODDTA].[Cam404].[PRODDTA].[f4211] sd where (sd.sdmcu) = li.limcu and sd.sditm = ib.ibitm                                     
                                       and sd.sddcto not in ('SA', 'ST', 'SQ') and sd.sdnxtr <> '999'), 0) +                                                                       
                                       isnull((select sum(lipcom) from [PRODDTA].[Cam404].[PRODDTA].[f41021] where (limcu) = li.limcu and left(ltrim(liglpt), 2) in ('CP', 'RM') and liitm = ibitm),0))  
                                 then                                                                                                                                              
                                      (isnull(case  when left(ibglpt, 2) = 'FG'                                                                                                  
                                     then(select sum(sd2.sdsoqs) from [PRODDTA].[Cam404].[PRODDTA].[f42119] sd2                                                                                           
                                        left outer join [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal2 on sd2.sddgl = cal2.juliandate                                                                   
                                          where sd2.sditm = ib.ibitm and(sd2.sdmcu) = li.limcu                                                                                     
                                     and cal2.[date] between(select convert(date,c.[date]) min_date                                                                                            
                             from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] c                                                                                                                           
                             join (select min(cal.calendarsmrt_key)                                                                                                                
                              calendarsmrt_key                                                                                                                                     
                              from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal                                                                                                                        
                             join (select year, month From [PRODDTA].[Cam404].[zpemjde].[calendar_dim]                                                                                                    
                              cal2                                                                                                                                                 
                             where 1 = 1                                                                                                                                           
                             and((select DateAdd(yy,-1,DateAdd(MM,-3,convert(Date,getdate()))))) = cal2.[date]) cal_sub                                                                                          
                             on cal.year = cal_sub.year    
                             and cal.month = cal_sub.month 
                             where 1 = 1   
                             and cal.number_working_days > 0) cal on                                                                                                               
                              c.calendarsmrt_key = cal.calendarsmrt_key) and(select distinct convert(date,getdate()) from [DataWarehouse].[dbo].[Calendar_dim]))                                              
                                 else (select (case  when iltrum = im.imuom1 then iltrqt                                                                                        
                                 else iltrqt / (select convert(decimal,(ucconv / 10000000)) from [PRODDTA].[Cam404].[PRODDTA].[f41003]                                                                           
                                         where ucum = im.imuom1 and ucrum = iltrum)                                                                                                
                                         end )*(-1)                                                                                                                                
                                 from [PRODDTA].[Cam404].[PRODDTA].[f4111]                                                                                                                                
                                 left outer                                                                                                                                        
                                 join [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal on iltrdj = cal.juliandate                                                                                          
                                  where ilitm = ib.ibitm                                                                                                                           
                                 and(ilmcu) = li.limcu and ildct = 'IM'                                                                                                            
                                 and cal.[date] between(select convert(Date,c.[date]) min_date                                                                                                 
                             from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] c                                                                                                                           
                             join (select min(cal.calendarsmrt_key)                                                                                                                
                              calendarsmrt_key                                                                                                                                     
                              from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal                                                                                                                        
                             join (select [year], [month] From [PRODDTA].[Cam404].[zpemjde].[calendar_dim]                                                                                                    
                              cal2                                                                                                                                                 
                             where 1 = 1                                                                                                                                           
                             and((select DateAdd(yy,-1,DateAdd(MM,-3,convert(Date,getdate())))))= cal2.[date]) cal_sub                                                                                           
                               on cal.[YEAR] = cal_sub.year                                                                                                                          
                             and cal.[month] = cal_sub.month                                                                                                                         
                             where 1 = 1                                                                                                                                           
                             and cal.number_working_days > 0) cal on                                                                                                               
                              c.calendarsmrt_key = cal.calendarsmrt_key) and(select distinct convert(date,getdate()) from [DataWarehouse].[dbo].[Calendar_dim]))                                              
                             end,0))/ 
							 --(isnull((days(convert(date,getdate())) - isnull(days(dba.juliantodate((select min(ioohdj) from [PRODDTA].[Cam404].[PRODDTA].[f4108] 
        --                             where (iomcu) = li.limcu and ioitm = ibitm))),                                                                                                
        --                             days(dba.juliantodate((select min(iltrdj) from [PRODDTA].[Cam404].[PRODDTA].[f4111] where (ilmcu) = li.limcu                                                         
        --                             and ibitm = ilitm)))) / 30),1))/ 15 * 36  
		(isnull((DateDiff(dd,(convert(date,getdate())),isnull((((select min(cal.[date]) from [PRODDTA].[Cam404].[PRODDTA].[f4108] 
								left outer join [DataWarehouse].[dbo].[Calendar_dim] cal
								on ioohdj=cal.juliandate
                                     where (iomcu) = li.limcu and ioitm = ibitm))),                                                                                                
                                     (((select min(iltrdj) from [PRODDTA].[Cam404].[PRODDTA].[f4111] 
									 left outer join [DataWarehouse].[dbo].[Calendar_dim] cal on iltrdj=cal.juliandate
									 where (ilmcu) = li.limcu                                                         
                                     and ibitm = ilitm))))) / 30),1))/15*36                                                                                                    
                                 else                                                                                                                                                    
                                     (isnull((select sum(sduorg) from [PRODDTA].[Cam404].[PRODDTA].[f4211] sd where ltrim(sd.sdmcu) = li.limcu and sd.sditm = ib.ibitm                                         
                                     and sd.sddcto not in ('SA', 'ST', 'SQ') and sd.sdnxtr <> '999'), 0) +                                                                               
                                     isnull((select sum(lipcom) from [PRODDTA].[Cam404].[PRODDTA].[f41021] where (limcu) = li.limcu and left(ltrim(liglpt), 2) in ('CP', 'RM') and liitm = ibitm),0))          
                                 end)-((36 * convert(decimal,(isnull(                                                                                                                               
                                     (select top 1 sum(lta2.amu)                                                                                                                              
                                 from [PRODDTA].[CAM404].[COLDFUSE].[TVLTA] lta2                                                                                                                                
                                 where lta2.itm = ib.ibitm and(lta2.mcu) = li.limcu                                                                                                      
                                 and lta2.an8 = 140991                                                                                                                                   
                                 and convert(date,lta2.rptDate) between(select convert(Date,c.[date]) min_date                                                                                             
                             from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] c                                                                                                                                 
                             join (select min(cal.calendarsmrt_key)                                                                                                                      
                              calendarsmrt_key                                                                                                                                           
                              from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal                                                                                                                              
                             join (select [year], [month] From [PRODDTA].[Cam404].[zpemjde].[calendar_dim]                                                                                                          
                              cal2                                                                                                                                                       
                             where 1 = 1                                                                                                                                                 
                             and(DateAdd(yy,-1,convert(Date,getdate()))) = cal2.[date]) cal_sub                                                                                                             
                             on cal.[year] = cal_sub.year                                                                                                                                  
                             and cal.[month] = cal_sub.month                                                                                                                               
                             where 1 = 1                                                                                                                                                 
                             and cal.number_working_days > 0) cal on                                                                                                                     
                              c.calendarsmrt_key = cal.calendarsmrt_key) and(select distinct convert(date,getdate()) from [DataWarehouse].[dbo].[Calendar_dim])                                                     
                                 group by  lta2.rptDate , lta2.itm, lta2.mcu                                                                                                             
                                 --order by lta2.rptdate desc                                                                                                                              
                                 ),0) 	+isnull((select top 1 sum(lta3.amu)                                                                                          
                                 from [PRODDTA].[CAM404].[COLDFUSE].[TVLTA] lta3                                                                                                                                
                                 where lta3.itm = ib.ibitm and(lta3.mcu) = li.limcu                                                                                                      
                                 and lta3.an8 = 141448                                                                                                                                   
                                 and convert(date,lta3.rptDate) between(select convert(Date,c.[date]) min_date                                                                                             
                             from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] c                                                                                                                                 
                             join (select min(cal.calendarsmrt_key)                                                                                                                      
                              calendarsmrt_key                                                                                                                                           
                              from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal                                                                                                                              
                             join (select year, month From [PRODDTA].[Cam404].[zpemjde].[calendar_dim]                                                                                                          
                              cal2                                                                                                                                                       
                             where 1 = 1                                                                                                                                                 
                             and(DateAdd(yy,-1,convert(Date,getdate()))) = cal2.[date]) cal_sub                                                                                                             
                             on cal.[year] = cal_sub.year                                                                                                                                  
                             and cal.[month] = cal_sub.month                                                                                                                               
                             where 1 = 1                                                                                                                                                 
                             and cal.number_working_days > 0) cal on                                                                                                                     
                              c.calendarsmrt_key = cal.calendarsmrt_key) and(select distinct convert(date,getdate()) from [DataWarehouse].[dbo].[Calendar_dim])                                                     
                                 group by  lta3.rptDate , lta3.itm, lta3.mcu                                                                                                             
                                 --order by lta3.rptdate desc                                                                                                                              
                                 ),0) +isnull((select top 1 sum(lta4.amu)                                                                                              
                                 from [PRODDTA].[CAM404].[COLDFUSE].[TVLTA] lta4                                                                                                                                
                                 where lta4.itm = ib.ibitm and(lta4.mcu) = li.limcu                                                                                                      
                                 and lta4.an8 = 142097                                                                                                                                   
                                and convert(Date,lta4.rptDate) between(select convert(date,c.[date]) min_date                                                                                              
                             from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] c                                                                                                                                 
                             join (select min(cal.calendarsmrt_key)                                                                                                                      
                              calendarsmrt_key                                                                                                                                           
                              from [PRODDTA].[Cam404].[zpemjde].[calendar_dim] cal                                                                                                                              
                             join (select year, month From [PRODDTA].[Cam404].[zpemjde].[calendar_dim]                                                                                                          
                              cal2                                                                                                                                                       
                             where 1 = 1                                                                                                                                                 
                             and(DateAdd(yy,-1,convert(Date,getdate()))) = cal2.[date]) cal_sub                                                                                                             
                             on cal.year = cal_sub.year                                                                                                                                  
                             and cal.month = cal_sub.month                                                                                                                               
                             where 1 = 1                                                                                                                                                 
                             and cal.number_working_days > 0) cal on                                                                                                                     
                              c.calendarsmrt_key = cal.calendarsmrt_key) and(select distinct convert(date,getdate()) from [DataWarehouse].[dbo].[Calendar_dim])                                                     
                                 group by  lta4.rptDate , lta4.itm, lta4.mcu                                                                                                             
                                 --order by lta4.rptdate desc                                                                                                                              
                                  ),0))))))                                                                                                                           
                                 else 0                                                                                                                                                  
                                  end))  ReserveQty                                                                                                                                       
                                          from  [PRODDTA].[Cam404].[PRODDTA].[f41021] li join [PRODDTA].[Cam404].[PRODDTA].[f4102] ib 
										  on li.limcu = ib.ibmcu and li.liitm = ib.ibitm                                                   
                                          join [PRODDTA].[Cam404].[PRODDTA].[f4101] im on ib.ibitm = im.imitm                                                                                                   
                             left join [PRODDTA].[Cam404].[PRODDTA].[f4105] co on ib.ibmcu = co.comcu and ib.ibitm = co.coitm and co.coledg = '07'                                                              
                                 where ltrim(li.limcu) in ('710000','730000')
                                 and iblitm not like '%*OP%'                                                                                                               
                                 and left(ibglpt,2) in ('FG', 'RM')                                                                                                        
                                 group by limcu, ibitm, iblitm, imdsc1, ibglpt, councs, imuom1, ibprp7                                                                     
                                 having sum(li.lipqoh) <> 0;