Use DataWarehouse
go
select                  ltrim(ib.ibmcu) as Division, 
                        ib.ibitm as Custodian, 
                        im.imdsc1 as Description, 
                        ltrim(ib.ibsrp1) as Type,
                        ltrim(ib.ibsrp2) as Group,
                        ltrim(ib.ibsrp3) as Code, 
                       --ltrim(ib.ibaitm) as Item, 
                    ltrim(ib.iblitm) as Item_Number			,coalesce(co.councs/1000000),0) as Unit_cost
			, coalesce(co.councs/1000000),0)*coalesce(sum(li.lipqoh),0) Extended_Cost
                        ,li.lilocn as locn                        ,ltrim(li.lilotn) as lot_number, 
                        coalesce(sum(li.lipqoh),0) as quantity_on_hand,
                        coalesce(sum(li.lipcom),0) as qty_soft_commit,
                        coalesce(SUM(LI.LIHCOM),0) as quantity_hard_commit,
                        coalesce(SUM(LI.LIFCOM),0) as quantity_future_commit,
                        SUM(LI.LIPQOH)-SUM(LI.LIPCOM)-SUM(LI.LIHCOM)-SUM(LI.LIFCOM) AS Avail_Qty,
                        coalesce(SUM(LI.LIQWBO),0) as Qty_workorder_receipt,
                        --coalesce(dec(co2.councs/1000000,15,7),0) as Last_In_Cost
from [PRODDTA].[Cam404].[proddta].[f4102] ib join [PRODDTA].[Cam404].[proddta].[f41021] li 
	on ib.ibmcu = li.limcu and ib.ibitm = li.liitm 
                              left join [PRODDTA].[Cam404].[proddta].[f4105] co 
							  on ib.ibitm = co.coitm and ib.ibmcu = co.comcu and co.coledg = '07' 
                              join [PRODDTA].[Cam404].[proddta].[f4101] im 
							  on ib.ibitm = im.imitm
                              left join [PRODDTA].[Cam404].[proddta].[f4105] co2 on ib.ibitm = co2.coitm and ib.ibmcu = co2.comcu and co2.coledg = '01'
where           1=1 --  left(trim(ib.ibmcu),2) = 'V4' 
--where             trim(ib.ibmcu) = '#mcu#'      
and                     LTRIM(ib.ibsrp1) in ('RM','FG','CP')
and               ib.ibstkt not in ('X')
group by  Ltrim(ib.ibmcu)
, ib.ibitm
, Ltrim(ib.iblitm) 
, Ltrim(ib.ibaitm)
, co.councs
, im.imdsc1
, ltrim(ib.ibsrp1)
, ltrim(ib.ibsrp2)
, ltrim(ib.ibsrp3)
, co2.councs
,li.lilocn
, ltrim(li.lilotn)