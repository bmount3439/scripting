use MoellerERP
go
update iT_Planning_Backlog set reportdate=convert(date,getdate())
update iT_Planning_Shipments set reportdate=convert(date,getdate())
update iT_Planning_WorkInProgress set reportdate=convert(date,getdate())
update iT_Sales_Bookings set reportdate=convert(date,getdate())