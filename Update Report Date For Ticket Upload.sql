use DataWarehouse
go
update iT_IT_CAMClosedTickets set REPORT_DATE='2019-03-03' where report_date is null;
update iT_IT_CAMNewTickets set REPORT_DATE='2019-03-03' where report_date is null;
update iT_IT_CAMOpenTickets set REPORT_DATE='2019-03-03' where report_date is null;

update iT_IT_CAMClosedTickets set REPORT_DATE='2018-12-23' where report_date='2018-12-25';
update iT_IT_CAMNewTickets set REPORT_DATE='2018-12-23' where report_date='2018-12-25';
update iT_IT_CAMOpenTickets set REPORT_DATE='2018-12-23' where report_date='2018-12-25';

delete from iT_IT_CAMClosedTickets where REPORT_DATE is null
delete from	iT_IT_CAMNewTickets where REPORT_DATE is null
delete from iT_IT_CAMOpenTickets where REPORT_DATE is null

delete from iT_IT_CAMClosedTickets where REPORT_DATE='2018-11-18'
delete from	iT_IT_CAMNewTickets where REPORT_DATE='2018-11-18'
delete from iT_IT_CAMOpenTickets where REPORT_DATE='2018-11-18'

delete from iT_IT_CAMClosedTickets where report_date='2018-09-03'
delete from iT_IT_CAMNewTickets where report_date='2018-09-03'
delete from iT_IT_CAMOpenTickets where report_date='2018-09-03'

truncate table sT_IT_CAMTickets;
insert into sT_IT_CAMTickets select * from tV_IT_CAMTickets;

select * from iT_IT_CAMClosedTickets where report_date is null
select * from iT_IT_CAMNewTickets  where report_date is null
select * from iT_IT_CAMOpenTickets where report_date is null

delete from iT_IT_CAMClosedTickets where report_date='2018-07-15'
delete from iT_IT_CAMNewTickets  where report_date='2018-07-15'
delete from iT_IT_CAMOpenTickets where report_date='2018-07-15'

use datawarehouse
go
truncate table sT_IT_CAMTickets;
insert into sT_IT_CAMTickets select * from tV_IT_CAMTickets;

delete from iT_IT_CAMOpenTickets where report_date is null;