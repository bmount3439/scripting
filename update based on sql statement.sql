Use SharePoint
go
insert into MASTER_COST (ITEM_NUMBER, [DESCRIPTION], [500_FACTOR],[1000_factor],[2500_FACTOR],[5000_FACTOR],[10000_FACTOR]
, [25000_FACTOR], [50000_FACTOR],[100000_FACTOR], LAST_EDIT_INITIALS, COMMENTS, LAST_EDIT_DATE)  
select ITEM_NUMBER, [DESCRIPTION], [500_FACTOR],[1000_FACTOR],[2500_FACTOR],[5000_FACTOR],[10000_FACTOR]
, [25000_FACTOR], [50000_FACTOR],[100000_FACTOR], LAST_EDIT_INITIALS, COMMENTS, GETDATE() From MASTER_COST_backup

--select * From master_cost_bACKUP
--drop table MASTER_COST

SELECT ITEM_NUMBER, [1000_Factor] 
FROM [SharePoint].[dbo].[MASTER_COST] WHERE [1000_FACTOR] IS NULL 
AND [500_FACTOR]<>[2500_FACTOR] AND LAST_EDIT_INITIALS IS NULL

update SP SET SP.[1000_FACTOR]=SP2.[1000 Factor] 
FROM [SharePoint].[dbo].[MASTER_COST] sp
join [SharePoint_test].[dbo].[Master_Cost_Sheet] sp2
on sp.ITEM_NUMBER=sp2.item_number
 WHERE sp.[1000_FACTOR] IS NULL 
AND sp.[500_FACTOR]<>sp.[2500_FACTOR] AND sp.LAST_EDIT_INITIALS IS NULL

use SharePoint
go
truncate table master_Cost

select factor_id,item_number, description, [500_FACTOR], [1000_FACTOR], [2500_FACTOR], [5000_FACTOR]
                                    , [10000_FACTOR], [25000_FACTOR], [50000_FACTOR], [100000_FACTOR], COMMENTS, LAST_EDIT_INITIALS, LAST_EDIT_DATE 
                                 from master_cost where Factor_id=1

								 SELECT * fROM MASTER_COST where 1000