use DataWarehouse
go
truncate table sT_Sales_OpenSalesOrders;
insert into sT_Sales_OpenSalesOrders
SELECT case
	when sales_orders.SDKCOO='00730' then '730000'
	when sales_orders.SDKCOO='00710' then '710000'
	when sales_orders.SDKCOO='00720' then '720000'
	when sales_orders.SDKCOO='00750' then '750000'
else sales_orders.sdkcoo
end division
, sales_orders.SDDOCO order_num
, sales_orders.SDLNID/1000 Line_Num 
, sales_orders.SDDCTO order_type
	--, sales_orders.SDPQOR Qty_Ordered 
	, sales_orders.SDUORG Qty_Ordered 
	, sales_orders.SDSOBK Qty_Backordered
, sales_orders.SDSOQS Qty_Shipped
, sales_orders.sdsocn Qty_Cancelled
, sales_orders.sdsone Qty_Future_Commit
--, sales_orders.sduopn Qty_Open
, sales_orders.SDITM item_id
, sales_orders.sdlitm item_num
, sales_orders.sdaitm item_num_second
, sales_orders.sdlnty line_type
--, sales_orders.SDADDJ actual_ship_date
,convert(date,convert(char(10),dateadd(day,convert(int,right(sales_orders.SDADDJ,3)),'01/01/'+left(right(sales_orders.SDADDJ,5),2))-1,101)) actual_ship_date
, (sales_orders.SDUORG-sales_orders.sdsocn-sales_orders.SDSOQS) remaining_qty--, sales_orders.*
	FROM [PRODDTA].[Cam404].[PRODDTA].[F4211] sales_orders
where 1=1 --sales_orders.sdkcoo='00730'
--and sales_orders.sddcto in ('SA','SO')
and sales_orders.sddcto='SO'
--and sales_orders.SDQTYT<>sales_orders.SDSOQS
--and sales_orders.sdsoqs<>sales_orders.sdpqor
--and (sales_orders.SDADDJ is not null and sales_orders.SDADDJ<>0)
and (sales_orders.SDUORG-sales_orders.sdsocn-sales_orders.SDSOQS+sales_orders.SDSOBK)>0
--and sales_orders.sduopn<>0;