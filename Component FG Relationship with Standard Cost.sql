truncate table iT_Eng_FinishedGoodsCompRelationship;
insert into iT_Eng_FinishedGoodsCompRelationship
SELECT  rtrim(cb.ixmmcu) business_unit
, cb.ixlitm comp_item
, cb.ixkitl fg_item
--, cb.ixitm parent_item_no
, cb.ixtbm bill_type 
, ib.ibaitm parent_item_no
, (cb.IXQNTY) qty_per
, (co.councs/1000000) as comp_standard_unit_cost 
FROM [PRODDTA].[Cam404].[PRODDTA].[F3002] cb 
	join dT_Corp_DivisionDefinitions div on
	ltrim(cb.ixmmcu)=div.division_id
	left outer join [PRODDTA].[Cam404].[PRODDTA].[f4102] ib on
	cb.ixitm=ib.ibitm
	and cb.ixmmcu=ib.ibmcu
	left join [PRODDTA].[Cam404].[PRODDTA].[f4105] co on 
	cb.IXKITL = co.COLITM
	and cb.ixmmcu = co.comcu 
	and co.coledg = '07' 
WHERE 1=1