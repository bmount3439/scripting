use datawarehouse
go
select  d.DIVISION, d.INVENTORY_TYPE
, ITEM_NUMBER
, quantity_on_hand
--, (quantity_soft_committed) qsc
--, (quantity_hard_committed) qhc
--, (quantity_on_future_commit) qofc
, selling_price_demand
--, cl.comp_item
from sT_Sales_ItemDemand d
		--, (select * from iT_Eng_FinishedGoodsCompRelationship where 1=1
		--	and ltrim(division)='730000') cl
where d.ITEM_NUMBER='BFN00340-3E1M'
and d.QUANTITY_ON_HAND>0
--and d.item_number=cl.fg_item
and d.INVENTORY_TYPE='FG'
--and (cl.comp_item like 'BH%' OR cl.comp_item like 'BF%' or cl.comp_item like 'BDC%' or cl.comp_item like 'BCN%')

--group by DIVISION, INVENTORY_TYPE, ITEM_NUMBER, SELLING_PRICE_DEMAND

--select * from iT_Eng_FinishedGoodsCompRelationship where 1=1
--and ltrim(division)='730000'

--select distinct division from iT_Eng_FinishedGoodsCompRelationship