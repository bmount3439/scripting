Use DataWarehouse
go
--drop view tV_Sales_DailyFlash
--create view tV_Sales_DailyFlash as 
select backlog.division
	, backlog.item_number
	, backlog.unit_of_measure
	, convert(Date,c.date) promised_ship_date
	, case
			when year(convert(date,backlog.promised_ship_date))>year(convert(date,getdate())) 
				and convert(int,((convert(varchar(2),right(convert(varchar(4),right(left(year(convert(date,getdate())),4),2)) + (
						case 
							when len(month(convert(date,getdate())))>1
							then convert(varchar(2),month(convert(date,getdate())))
							else ('0'+convert(varchar(1),month(convert(date,getdate()))))
						end)+12,2)-13) + right(year(getdate()),2))+1)) --current month val
						<
						convert(int,convert(varchar(2),right(convert(varchar(4),right(left(year(convert(date,backlog.PROMISED_SHIP_DATE)),4),2)) + (
						case 
							when len(month(convert(date,backlog.PROMISED_SHIP_DATE)))>1
							then convert(varchar(2),month(convert(date,backlog.PROMISED_SHIP_DATE)))
							else ('0'+convert(varchar(1),month(convert(date,backlog.PROMISED_SHIP_DATE))))
						end)+12,2)-12) + right(year(backlog.PROMISED_SHIP_DATE),2)) --promised date val
					then 'OUT OF RANGE'
			when year(convert(date,backlog.promised_ship_date))>year(convert(date,getdate())) 
				and convert(int,((convert(varchar(2),right(convert(varchar(4),right(left(year(convert(date,getdate())),4),2)) + (
						case 
							when len(month(convert(date,getdate())))>1
							then convert(varchar(2),month(convert(date,getdate())))
							else ('0'+convert(varchar(1),month(convert(date,getdate()))))
						end)+12,2)-13) + right(year(getdate()),2))+1)) --current month val
						>=
						convert(int,convert(varchar(2),right(convert(varchar(4),right(left(year(convert(date,backlog.PROMISED_SHIP_DATE)),4),2)) + (
						case 
							when len(month(convert(date,backlog.PROMISED_SHIP_DATE)))>1
							then convert(varchar(2),month(convert(date,backlog.PROMISED_SHIP_DATE)))
							else ('0'+convert(varchar(1),month(convert(date,backlog.PROMISED_SHIP_DATE))))
						end)+12,2)-12) + right(year(backlog.PROMISED_SHIP_DATE),2)) --promised date val
					then 'ON TIME'
					--when backlog.ship_year=year(convert(date,getdate())) 
						when convert(date,backlog.promised_ship_date)>=convert(date,getdate())
					then
					'ON TIME'
			else 'PAST DUE'
			--need a clause to handle the current date
			END Promised_Ship_Date_Axis
			,convert(int,convert(varchar(2),right(convert(varchar(4),right(left(year(convert(date,backlog.PROMISED_SHIP_DATE)),4),2)) + (
						case 
							when len(month(convert(date,backlog.PROMISED_SHIP_DATE)))>1
							then convert(varchar(2),month(convert(date,backlog.PROMISED_SHIP_DATE)))
							else ('0'+convert(varchar(1),month(convert(date,backlog.PROMISED_SHIP_DATE))))
						end)+12,2)-12) + right(year(backlog.PROMISED_SHIP_DATE),2)) promised_ship_date_val
			--what month am I in?
			--, case
			--	when 
			--		case when right(convert(varchar(4),right(left(year(convert(date,backlog.PROMISED_SHIP_DATE)),4),2)) + (case 
			--			when len(month(convert(date,backlog.PROMISED_SHIP_DATE)))>1
			--			then convert(varchar(2),month(convert(date,backlog.PROMISED_SHIP_DATE)))
			--			else ('0'+convert(varchar(1),month(convert(date,backlog.PROMISED_SHIP_DATE))))
			--			end)+12,2)>12
			--			then convert(varchar(2),right(convert(varchar(4),right(left(year(convert(date,backlog.PROMISED_SHIP_DATE)),4),2)) + (case 
			--			when len(month(convert(date,backlog.PROMISED_SHIP_DATE)))>1
			--			then convert(varchar(2),month(convert(date,backlog.PROMISED_SHIP_DATE)))
			--			else ('0'+convert(varchar(1),month(convert(date,backlog.PROMISED_SHIP_DATE))))
			--			end)+12,2)-12) + right(year(backlog.PROMISED_SHIP_DATE),2)
			--		end not like right(year(convert(date,getdate())),2)
			--	then 
			--		case when right(convert(varchar(4),right(left(year(convert(date,backlog.PROMISED_SHIP_DATE)),4),2)) + (
			--				case 
			--					when len(month(convert(date,backlog.PROMISED_SHIP_DATE)))>1
			--					then convert(varchar(2),month(convert(date,backlog.PROMISED_SHIP_DATE)))
			--					else ('0'+convert(varchar(1),month(convert(date,backlog.PROMISED_SHIP_DATE))))
			--				end)+12,2)>12
			--			then convert(varchar(2),right(convert(varchar(4),right(left(year(convert(date,backlog.PROMISED_SHIP_DATE)),4),2)) + (case 
			--			when len(month(convert(date,backlog.PROMISED_SHIP_DATE)))>1
			--			then convert(varchar(2),month(convert(date,backlog.PROMISED_SHIP_DATE)))
			--			else ('0'+convert(varchar(1),month(convert(date,backlog.PROMISED_SHIP_DATE))))
			--		end)+12,2)-12) + right(year(backlog.PROMISED_SHIP_DATE),2)
			--			--???
			--		when right(convert(varchar(4),right(left(year(convert(date,backlog.PROMISED_SHIP_DATE)),4),2)) + (
			--			case 
			--				when len(month(convert(date,backlog.PROMISED_SHIP_DATE)))>1
			--				then convert(varchar(2),month(convert(date,backlog.PROMISED_SHIP_DATE)))
			--				else ('0'+convert(varchar(1),month(convert(date,backlog.PROMISED_SHIP_DATE))))
			--			end)+12,2)>12
			--		then convert(varchar(2),right(convert(varchar(4),right(left(year(convert(date,backlog.PROMISED_SHIP_DATE)),4),2)) + (
			--			case 
			--				when len(month(convert(date,backlog.PROMISED_SHIP_DATE)))>1
			--				then convert(varchar(2),month(convert(date,backlog.PROMISED_SHIP_DATE)))
			--				else ('0'+convert(varchar(1),month(convert(date,backlog.PROMISED_SHIP_DATE))))
			--			end)+12,2)-12) + right(year(backlog.PROMISED_SHIP_DATE),2)
			--		else 0
			--	end
			--end promised_date_month_val
			--, convert(varchar(2),right(convert(varchar(4),right(left(year(convert(date,getdate())),4),2)) + (
			--			case 
			--				when len(month(convert(date,getdate())))>1
			--				then convert(varchar(2),month(convert(date,getdate())))
			--				else ('0'+convert(varchar(1),month(convert(date,getdate()))))
			--			end)+12,2)-12)-1 month_val
			,((convert(varchar(2),right(convert(varchar(4),right(left(year(convert(date,getdate())),4),2)) + (
						case 
							when len(month(convert(date,getdate())))>1
							then convert(varchar(2),month(convert(date,getdate())))
							else ('0'+convert(varchar(1),month(convert(date,getdate()))))
						end)+12,2)-13) + right(year(getdate()),2))+1) max_month_val
	, backlog.ship_year
	, backlog.ship_period
	, backlog.business_days
	, backlog.assemblies_ordered
	, backlog.units_ordered
--	, backlog.*
from iT_Sales_SalesBacklog backlog
	full outer join Calendar_Dim c on 
	convert(Date,c.date)=convert(date,backlog.promised_ship_date)
	where 1=1
	--and ship_year=2019
	