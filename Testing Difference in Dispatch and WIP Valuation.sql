USE [MoellerERP]
GO

select * From  tV_WIP_WorkInProgress where Job='7691'
use MoellerERP
go
select * From iT_JCPROC where jonum9='7691' and issu19='7/23/2018'
select * From iT_OPENJOBS where JOBNO='7691' and ISSUED='7/23/2018'



--Create View [dbo].[tV_WIP_WorkInProgress] as 
select oj.jonum Job, convert(date,oj.issued) IssuedDate, replace(concat(oj.jonum,convert(date,oj.issued)),'-','') NewJobID, oj.pn,oj.lastop, oj.nextop,oj.totalop
, oj.reqwt14 ReqWeight, oj.finwt14 FinalWeight, oj.hours14 TotalHours --, oj.totcost14 TotalCost
, oj.REQWT14
, ((convert(decimal,oj.REQWT14)/1000)*convert(decimal,oj.MATLCOST))*rh.Quantity MatCost
--, oj.MATLCOST MaterialCost
, oj.matcostper MatCostPer
, oj.killdate KillDate --, oj.*
--, (rh.operation_cost_per_ea) TotalCost_By_IssueDate
, sum(rh.operation_cost_per_ea) Total_Op_Cost_By_IssueDate
, ((convert(decimal,oj.REQWT14)/1000)*convert(decimal,oj.MATLCOST))*rh.Quantity+sum(rh.operation_cost_per_ea) TotalCost_By_IssueDate
--add material cost per lb to toatl cost by issue date
, rh.quantity Quantity
From iT_OPENJOBS oj 
left outer join sT_Routing_OperationRouterHistory rh
on oj.jonum=rh.job
and replace(concat(oj.jonum,convert(date,oj.issued)),'-','')=rh.NewJobID
and rh.Quantity=oj.QT
where 1=1 
and jonum='7691'
and convert(date,oj.issued)='11/6/2018'
and ltrim(oj.jonum)<>''
group by oj.jonum, convert(date,oj.issued), replace(concat(oj.jonum,convert(date,oj.issued)),'-',''), oj.pn,oj.lastop, oj.nextop,oj.totalop
, oj.reqwt14, oj.finwt14, oj.hours14 --, oj.totcost14 TotalCost
, oj.matcostper, oj.killdate, rh.Quantity,oj.REQWT14, oj.MATLCOST



