Use DataWarehouse
go

--what is today
select dateadd(day,-1,convert(Date,getdate())) todays_date
--last production day
select max(convert(date,[date])) from calendar_Dim where weekend<>'Y' and holiday<>'Y' and convert(date,[date])<=DateAdd(day,-1,convert(date,getdate()))
--start of current week
select min(convert(date,[date])) 
from calendar_dim cal 
where 1=1
and cal.fiscal_week=(select fiscal_week from calendar_Dim where convert(date,[date])=DateAdd(day,-1,convert(date,getdate())))
and cal.fiscal_year=(select fiscal_year from calendar_Dim where convert(date,[date])=DateAdd(day,-1,convert(date,getdate())))
--start of current fiscal month
select min(convert(date,[date])) 
from calendar_dim cal 
where 1=1
and cal.fiscal_month=(select fiscal_month from calendar_Dim where convert(date,[date])=DateAdd(day,-1,convert(date,getdate())))
and cal.fiscal_year=(select fiscal_year from calendar_Dim where convert(date,[date])=DateAdd(day,-1,convert(date,getdate())))
--start of current fiscal quarter


select * From calendar_dim where fiscal_Week=50 and fiscal_year='2018'
select * from calendar_Dim where convert(date,[date])=DateAdd(day,-1,convert(date,getdate()))