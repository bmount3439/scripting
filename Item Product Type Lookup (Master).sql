USE DataWarehouse
Go
select imitm item_num
 , im.imlitm item_num2
--, imaitm item_num3
--, imdsc1 item_description_1
--, imdsc2 item_description_2
--, imsrp1 sales_code_1
, im.imsrp2 PRODUCT_LINE
--, code2.product_code_desc product_family
, im.imsrp3 PRODUCT_CLASS
, im.imsrp4 PRODUCT_FAMILY
--, im.imsrp5 sales_code_5
, code.product_code_desc product_class --product family
--, im.*
from [PRODDTA].[Cam404].[PRODDTA].[F4101] im
left outer join (SELECT lookup_codes.drky product_code
	, lookup_codes.drdl01 product_code_desc
 FROM [PRODDTA].[Cam404].[PRODDTA].[F0005] lookup_codes WHERE 1=1
AND lookup_codes.DRSY='41'
AND lookup_codes.DRRT='S3') code on 
	ltrim(im.imsrp3)=ltrim(code.product_code)
left outer join (SELECT lookup_codes.drky product_code
			, lookup_codes.drdl01 product_code_desc
		 FROM [PRODCTL].[Cam404].[PRODCTL].[F0005] lookup_codes WHERE 1=1
		AND lookup_codes.DRSY='41'
		AND lookup_codes.DRRT='S2') code2 on 
		ltrim(im.imsrp2)=ltrim(code2.product_code)
WHERE 1=1
--and im.imlitm='BFN00340-3E2CM'