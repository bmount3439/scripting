use DataWarehouse
go
select   wa.wadoco
, wa.wauorg  ordered_qty
, wa.walnid  line_num
, wa.warcto  related_so_type
, wa.warorn related_so
, wa.waitm  item_num
, ltrim(wa.wamcu ) division
, wa.warkco company
,case 	when warorn <> ''   --when the related so is null
		then 0
			--coalesce(wauorg*(select top 1 sduprc*.000001 
			--from  [PRODDTA].[Cam404].[proddta].[f4211]   --take the order qty and multiply by unit price from the sales orders
			--where sdkcoo = warkco --where company matches
			-- and (sdlnid*.001,0) = (walnid*.001,0)   --and the line numbers match from the sales order to line number on the workorder
			--and (sddoco) = warorn --and the order number matches the related so lookup
			--and sddcto = warcto --and the order types match
			--and sdnxtr <> '999' --and operation isnt complete
			--order by sdlnid --order by the line number and grab only the first line
			----fetch first row only
			--),0)
else case 	when coalesce((select top 1 sum(sduorg) --select the order quantity
		from  [PRODDTA].[Cam404].[proddta].[f4211]  --sales orders
		where sdmcu = wa.wamcu --where division matches
		and sditm = wa.waitm --and items match
			and sdnxtr <> '999' --and workorder is not complete
			and sddcto = 'SO'  --and order type is SO
		group by sddoco --order num
			, sdppdj --promised shipment date
		order by sdppdj --order by promised shipment asc
			) ,0) >= wa.wauorg then  --grab one row and make sure that the quantity that was ordered is greater than what was is in the workorder
			wa.wauorg*coalesce((select top 1 sduprc*.000001 
				from  [PRODDTA].[Cam404].[proddta].[f4211]  --multiply the quantity ordered on the workorder by the price per unit
				where sditm = wa.waitm --where the items match
				and sdmcu = wa.wamcu -- and the divisions match
				and sdnxtr <> '999'  --and the sales order is not closed
				and sddcto = 'SO'  --and the order type is SO
				order by sdppdj --order by promised ship date
				, sddoco --followed by ordering by order num
				--fetch first row only
				) ,0) --get only the first row returned
		when coalesce((select top 1 sum(sduorg) --get quantity ordered from so
			from [PRODDTA].[Cam404].[proddta].[f4211]
				where sdmcu = wa.wamcu --where the divisions match
				and sditm = wa.waitm --and the items match
				and sdnxtr <> '999' --and the order isn't closed
				and sddcto = 'SO' -- and the order type is SO
				group by sddoco --group by order num
					, sdppdj --and promised ship date
				order by sdppdj  --order by promised ship date
				--fetch first row only
				) ,0) <= wa.wauorg  --get the first row only and get the qty on the workorder
					then coalesce((select top 1 sum(sduorg) as sduorg --qty ordered
						from  [PRODDTA].[Cam404].[proddta].[f4211]
						where sdmcu = wa.wamcu --divisions match
						and sditm = wa.waitm --items match
						and sdnxtr <> '999' 
						and sddcto = 'SO' 
						group by sddoco --group by order num
							, sdppdj --and promised ship date
						order by sdppdj 
						--fetch first row only
						) ,0)* --order by promised ship date and return only one row
						coalesce((select top 1 sduprc*.000001 
							from  [PRODDTA].[Cam404].[proddta].[f4211]  --multiply the price per piece
							where sditm = wa.waitm --items match
							and sdmcu = wa.wamcu --divisions match
							and sdnxtr <> '999' --so isn't closed
							and sddcto = 'SO' --so is type so
							order by sdppdj, --order by promised ship date
								 sddoco --followed by order num
								--fetch first row only
								) ,0) --get one record
else	0
end
end as revAmt
FROM [PRODDTA].[Cam404].[proddta].[f4801] wa
where wa.wadoco='617890'
