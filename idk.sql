select 
	 bookings.division
	, bookings.item_id
	, bookings.item_num
	--, bookings.bookdate
	--, bookings.promisedate
	, bookings.shipFY
	, bookings.shipPeriod
	--, convert(int,bookings.bqty) Qty
	, convert(decimal,bookings.bamt) Amount
	, bookings.bdoco
	, bookings.blnid
	--, (sum(convert(decimal,std_cost.sduncs))*sum(convert(int,bookings.bqty))) stdCost
	from
	(select --avg(ifnull(convert(decimal,sd1.sduncs),convert(decimal,sd2.sduncs))/1000000) sduncs --unit cost amount
	--, 
	TRIM(ifnull(sd1.sdkcoo,sd2.sdkcoo)) sdkcoo --order company order num
	, ifnull(sd1.sddoco,sd2.sddoco) order_num --order num
	, ifnull(sd1.sddcto,sd2.sddcto) order_type --order type
	, ifnull(sd1.sdlnid,sd2.sdlnid) line_num --line num
				from PRODDTA.f4211 sd1 
				full outer join PRODDTA.f42119 sd2 on --hist table
				TRIM(sd1.sdkcoo)=TRIM(sd2.sdkcoo)
				and sd1.sddoco=sd2.sddoco 
				and sd1.sddcto=sd2.sddcto
				and sd1.sdlnid=sd2.sdlnid
				group by TRIM(ifnull(sd1.sdkcoo,sd2.sdkcoo)) 
				, ifnull(sd1.sddoco,sd2.sddoco)
				, ifnull(sd1.sddcto,sd2.sddcto)
				, ifnull(sd1.sdlnid,sd2.sdlnid)
            ) std_cost
join (select  case
			when TRIM(b.bco) ='00730' then '730000'
			when TRIM(b.bco)='00750' then '750000'
			when TRIM(b.bco)='00710' then '710000'
			when TRIM(b.bco)='00720' then '720000'
			end Division
			, b.bco
		, b.bitm item_id
		, TRIM(b.blitm) as item_num 
	--	, convert(date,convert(char(10),dateadd(day,convert(int,right(b.bupmj,3)),'01/01/'+left(right(b.bupmj,5),2))-1,101)) as bookdate
	--	, convert(date,convert(char(10),dateadd(day,convert(int,right(b.bpddj,3)),'01/01/'+left(right(b.bpddj,5),2))-1,101)) as promisedate
		, c.fiscalYear as fYear
		, c.fiscalMonth as fMonth
        , sp.fiscalyear as shipFY
		, sp.fiscalmonth as shipFP
		,left(sp.calendarsmrt_key,6) as shipPeriod
       -- ,sum(convert(int,b.bqty)) as bqty
        , sum(convert(decimal,b.bamt)/100) as bamt
		--, sum(b.bdoco) as bdoco
		, b.bdoco as bdoco --order number
		--, sum(convert(decimal,b.blnid)) as blnid
		, b.blnid as blnid--line number
		, b.bdcto  --order type
      from        ZPEMJDE.BOOKINGS b join Calendar_Dim c on b.bupmj = c.juliandate 
                              left join zpemjde.Calendar_Dim sp on b.bpddj = sp.juliandate 
                              left outer join PRODDTA.f4105 co on b.bitm = co.coitm 
									and b.bmcu = co.comcu and co.coledg = '07' 
      where   1=1 
      and               TRIM(b.blnty) not in ('T')
     and               TRIM(b.bdcto) not in ('SA','ST','SQ')  
      group by    b.bco
	  , b.bmcu
	  , b.bitm
	  , b.blitm
	  , b.bdcto
	 --, convert(date,convert(char(10),dateadd(day,convert(int,right(b.bupmj,3)),'01/01/'+left(right(b.bupmj,5),2))-1,101))
	--,convert(date,convert(char(10),dateadd(day,convert(int,right(b.bpddj,3)),'01/01/'+left(right(b.bpddj,5),2))-1,101))
	  , c.fiscalYear
	  , c.fiscalMonth 
      , sp.fiscalyear
	, sp.fiscalmonth
	--, convert(co.councs,decimal)
	, b.bdoco  --order number
	, b.blnid--line number
	,left(sp.calendarsmrt_key,6)
	) bookings on
		bookings.bco = std_cost.sdkcoo
		and bookings.bdoco = std_cost.order_num 
		and bookings.bdcto = std_cost.order_type
		and bookings.blnid = std_cost.line_num
		group by 
		 bookings.division
		, bookings.item_id
		, bookings.item_num
	--	, bookings.bookdate
	--	, bookings.promisedate
		, bookings.fYear
		, bookings.fMonth
		, bookings.shipFY
		, bookings.shipPeriod
		--, convert(int,bookings.bqty)
		, convert(decimal,bookings.bamt)
		, bookings.bdoco
		, bookings.blnid;