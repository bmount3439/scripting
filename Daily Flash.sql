use datawarehouse
go
select            sd.sdkcoo, sd.sdmcu, sd.sditm, sd.sdlitm, 
                        sd.sdivd, dba.juliantodate(sd.sdivd) as invDate, 
                        sd.sdppdj, dba.juliantodate(sd.sdppdj) as promShipDate,
                        c.fiscalYear as fYear, c.fiscalMonth as fMonth,
                        sp.fiscalyear as shipFY, sp.fiscalmonth as shipFP,
                        (sp.fiscalyear*100)+sp.fiscalmonth as shipPeriod,
                        coalesce(sum(sd.sdsoqs),0) as sdsoqs,
                        coalesce(sum(sd.sdaexp/100),0) as sdaexp,   
                        coalesce(sum(sd.sdecst/100),0) as sdecst,   
                        coalesce(sum(sd.sdsoqs)*co.councs/1000000,0) as stdCost
						,sd.sdivd julian_date
      from         [PRODDTA].[Cam404].[PRODDTA].[f42119] sd join v_Calendar_Dim c on sd.sdivd = c.juliandate 
                              join v_Calendar_Dim sp on sd.sdppdj = sp.juliandate 
                              left outer join  [PRODDTA].[Cam404].[PRODDTA].[f4105] co on sd.sditm = co.coitm and sd.sdmcu = co.comcu and co.coledg = '07' 
      where       1=1 --sd.sdkcoo = '#co#'
      and               sd.sddcto not in ('SA','SB','ST','SQ') 
      and               sd.sdlnty not in ('T')                 
      --and               sd.sdivd between dba.datetojulian(date('#DateFormat(getStartDate.startDate,"yyyy-mm-dd")#'))
                                    --and      dba.datetojulian(date('#DateFormat(asOfDate,"yyyy-mm-dd")#'))           
      group by    sd.sdkcoo, sd.sdmcu, sd.sditm, sd.sdlitm, sd.sdivd, sd.sdppdj, 
                        c.fiscalYear, c.fiscalMonth, sp.fiscalyear, sp.fiscalmonth, co.councs
