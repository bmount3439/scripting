Insert Into iT_WIP_WorkordersInExcessOfDemand
select wammcu Division
 , wadoco WORKORDER
,  waitm
, trim(walitm) as ITEM_NUMBER
, trim(wadl01) as DESCRIPTION
 				 , wauorg QTY
				 ,(co.councs*.000001) as UNIT_COST
				 , wauorg*(co.councs*.000001) EXTENDED_COST
				 --,  dba.juliantodate(watrdj) as watrdj
				 ,convert(date,convert(char(10),dateadd(day,convert(int,right(watrdj,3)),'01/01/'+left(right(watrdj,5),2))-1,101)) WO_START_DATE
				 --, 				dba.juliantodate(wadrqj) as wadrqj
				 ,convert(date,convert(char(10),dateadd(day,convert(int,right(wadrqj,3)),'01/01/'+left(right(wadrqj,5),2))-1,101)) REQUEST_DATE
				 --, dba.juliantodate(wastrt) as wastrt
				 ,convert(date,convert(char(10),dateadd(day,convert(int,right(wastrt,3)),'01/01/'+left(right(wastrt,5),2))-1,101)) CREATION_DATE
				 , 				sum(li.lipqoh) as QTY_ON_HAND
				 , 						ibsafe SAFETY
				--,sum(li.lipcom) as pcom<!--- so/wo soft commit --->
				--,sum(li.lihcom) as hcom<!--- so hard commit --->
				--,sum(li.lifcom) as fcom<!--- future commit --->
				--,sum(li.lifun1) as fun1<!--- work order soft commit --->
				--,sum(li.liqowo) as qowo<!--- work order hard commit --->
				--,sum(li.lipqoh)-sum(li.lipcom)-sum(li.lihcom)-sum(li.lifcom)-sum(li.liqowo)-sum(li.lifun1) as avail
		--,		sum(li.liqwbo) as qwbo <!--- on work order --->
		--,		sum(li.lipreq) as preq <!--- on purchase order --->
		--demand
		, sum(li.lipcom)+sum(li.lifcom)+sum(li.lihcom)+sum(li.liqowo)+coalesce(	(select sum(mfuorg) from [PRODDTA].[Cam404].[proddta].[f3460]
						where mfitm = ib.ibitm and mfmcu = ib.ibmcu and mftypf = 'BF' 
						and convert(date,convert(char(10),dateadd(day,convert(int,right(mfdrqj,3)),'01/01/'+left(right(mfdrqj,5),2))-1,101))>convert(date,getdate())
						,0) demand
		--supply
		, sum(li.lipreq)+sum(li.liqwbo) supply
		--excess
		, sum(li.lipqoh)-sum(li.lipcom)-sum(li.lihcom)-sum(li.lifcom)-sum(li.liqowo)+sum(li.lipreq)+sum(li.liqwbo)-
				 coalesce(	(select sum(mfuorg) from [PRODDTA].[Cam404].[proddta].[f3460]
						where mfitm = ib.ibitm and mfmcu = ib.ibmcu and mftypf = 'BF' 
						and convert(date,convert(char(10),dateadd(day,convert(int,right(mfdrqj,3)),'01/01/'+left(right(mfdrqj,5),2))-1,101))>convert(date,getdate())
						,0) excess
	from 		 [PRODDTA].[Cam404].[proddta].[f4801] wa join  [PRODDTA].[Cam404].[proddta].[f4102] ib 
	on wa.wammcu = ib.ibmcu and
wa.waitm = ib.ibitm 
					join  [PRODDTA].[Cam404].[proddta].[f41021] li on ib.ibmcu = li.limcu 
					and ib.ibitm = li.liitm
					left join  [PRODDTA].[Cam404].[proddta].[f4105] co 
					on ib.ibitm = co.coitm and ib.ibmcu = co.comcu and co.coledg = '07'
	where	--	trim(ib.ibmcu) = '#mcu#'
	and			left(ib.ibglpt,1) <> 'T'	<!--- Exclude tooling. --->
	--and 		watrdj = dba.datetojulian(current date)
	and convert(date,convert(char(10),dateadd(day,convert(int,right(watrdj,3)),'01/01/'+left(right(watrdj,5),2))-1,101))=convert(date,getdate())
	and 		wasrst <> '99'
	group by	wammcu, wadoco, waitm, walitm, wadl01, wauorg, watrdj, wadrqj,
wastrt, ib.ibmcu ,ib.ibitm ,ib.iblitm 
				,ibsafe, councs
	having		sum(li.lipqoh)+sum(li.lipcom)+sum(li.lihcom)+sum(li.lifcom)+sum(li.liqwbo)
					+sum(li.lipreq)+sum(li.liqowo)+sum(li.lifun1) <> 0
	order by	wadoco 
