USE [DataWarehouse]
GO
--drop view tV_IT_CAMTickets
--create view [dbo].[tV_IT_CAMTickets] AS
select isnull(ct.ticket_id, hist.ticket_id) ticket_id
	, isnull(ct.type,hist.type) type
	, isnull(ct.PRIORITY,hist.PRIORITY) priority
	, isnull(ct.CATEGORY,hist.CATEGORY) category
	, isnull(ct.transaction_date,hist.transaction_date) transaction_date
	, isnull(ct.owner, hist.owner) owner
	, isnull(ct.SUBMITTER,hist.submitter) submitter
	, isnull(ct.COMPANY,hist.COMPANY) company
	, isnull(ct.DEPARTMENT,hist.DEPARTMENT) department
	, isnull(ct.title,hist.title) title
	, isnull(ct.Ticket_Type,hist.ticket_type) Ticket_Type
	, isnull(ct.current_date_stamp,hist.current_date_stamp) current_date_stamp
	, isnull(ct.creation_date,(select min(convert(date,created)) from iT_IT_CAMOpenTickets where ticket_id=hist.ticket_id)) creation_date
	, isnull(ct.start_week_num,(select ct.start_week_num from tV_IT_CAMTickets_test b where b.ticket_id=ct.ticket_id)) start_week_num
	, ct.ticket_week_num
	, ct.ticket_age
	, ct.bucket
	, ct.Aging
From (select distinct tx.ticket_id
	, tx.type
	, tx.priority
	, tx.category
	, tx.transaction_date
	, tx.owner
	, tx.submitter
	, tx.company
	, tx.department
	, tx.title
	, case 
		when (select distinct case 
						when h.STATUS<>'Closed' then 'OPEN'
						when h.status='Closed' then 'CLOSED'
						ELSE 'OPEN'
						end Agg_Status
from tV_IT_CAMTicketsHistory h
, (select sub_bucket.this_week 
	, dateadd(day,-7,sub_bucket.this_week)  wk1 --dateadd(day,-7,bucket.this_week)
	, dateadd(day,-14,sub_bucket.this_week) wk2
	, dateadd(day,-21,sub_bucket.this_week) wk3
	, dateadd(day,-28,sub_bucket.this_week) wk4
	, dateadd(day,-35,sub_bucket.this_week) wk5
	, dateadd(day,-42,sub_bucket.this_week) wk6
	, dateadd(day,-49,sub_bucket.this_week) wk7
	, dateadd(day,-56,sub_bucket.this_week) wk8
	, dateadd(day,-63,sub_bucket.this_week) wk9
	, dateadd(day,-70,sub_bucket.this_week) wk10
	, dateadd(day,-77,sub_bucket.this_week) wk11
	, dateadd(day,-84,sub_bucket.this_week) wk12
	From (select case
	when datename(weekday,convert(date,getdate()))='Sunday' then convert(date,getdate()+6)
	when datename(weekday,convert(date,getdate()))='Monday' then convert(date,getdate()+5)
	when datename(weekday,convert(date,getdate()))='Tuesday' then convert(date,getdate()+4)
	when datename(weekday,convert(date,getdate()))='Wednesday' then convert(date,getdate()+3)
	when datename(weekday,convert(date,getdate()))='Thursday' then convert(date,getdate()+2)
	when datename(weekday,convert(date,getdate()))='Friday' then convert(date,getdate()+1)
	when datename(weekday,convert(date,getdate()))='Saturday' then convert(date,getdate())
	else ''
	end this_week) sub_bucket) wk
	where 1=1
	and dateadd(day,-1,h.report_date)=wk.wk1
	and h.ticket_id=tx.ticket_id
	)='OPEN' 
		then 'OPEN'
		else 'CLOSED'
		end Ticket_Type
	--, tx.Ticket_Type
	, tx.current_Date_stamp
	--, isnull(new.creation_date,tx.transaction_date) creation_date
	, new.creation_date creation_date
	, tx.start_week_num
	, tx.ticket_week_num
	, tx.current_week_num
	, tx.ticket_age
	, tx.bucket
	, tx.aging
	--added
	--, tx.report_date
	--, dups.repeat_tix
from (select distinct ticket_queue.ticket_id
	, ticket_queue.type
	, ticket_queue.PRIORITY
	--, ticket_queue.STATUS
	, ticket_queue.CATEGORY
	, ticket_queue.transaction_date
	, ticket_queue.owner
	, ticket_queue.SUBMITTER
	, ticket_queue.company
	, ticket_queue.DEPARTMENT
	, ticket_queue.title
	, ticket_queue.Ticket_Type
	, ticket_queue.current_date_stamp
	, ticket_queue.start_week_num
	, ticket_queue.ticket_week_num
	, ticket_queue.current_week_num
	, ticket_queue.ticket_age
	, max(ticket_queue.REPORT_DATE) report_date
, case	
	when convert(date,transaction_date)>=bucket.this_week then 'Not applicable'
	when convert(date,transaction_date)<=bucket.wk1 and convert(date,transaction_date)>bucket.wk2 then wk1
	when convert(date,transaction_date)<=bucket.wk2 and convert(date,transaction_date)>bucket.wk3 then wk2
	when convert(date,transaction_date)<=bucket.wk3 and convert(date,transaction_date)>bucket.wk4 then wk3
	when convert(date,transaction_date)<=bucket.wk4 and convert(date,transaction_date)>bucket.wk5 then wk4
	when convert(date,transaction_date)<=bucket.wk5 and convert(date,transaction_date)>bucket.wk6 then wk5
	when convert(date,transaction_date)<=bucket.wk6 and convert(date,transaction_date)>bucket.wk7 then wk6
	when convert(date,transaction_date)<=bucket.wk7 and convert(date,transaction_date)>bucket.wk8 then wk7
	when convert(date,transaction_date)<=bucket.wk8 and convert(date,transaction_date)>bucket.wk9 then wk8
	when convert(date,transaction_date)<=bucket.wk9 and convert(date,transaction_date)>bucket.wk10 then wk9
	when convert(date,transaction_date)<=bucket.wk10 and convert(date,transaction_date)>bucket.wk11 then wk10
	when convert(date,transaction_date)<=bucket.wk11 and  convert(date,transaction_date)>bucket.wk12 then wk11
	when convert(date,transaction_date)<=bucket.wk12 and  convert(date,transaction_date)>bucket.this_week then wk12
	end bucket
	, case		
		when ticket_age<7 then '<7 Days'
		when ticket_age<=14 and ticket_age>=7 then '7-14 days'
		when ticket_age<=28 and ticket_age>14 then '15-28 days'
		--when ticket_age<28 and ticket_age>=21 then '21-27 days'
		when ticket_age>28 then '>28 days'
		else ''
		end Aging
FROM (select distinct new.ticket_id, new.Type,new.PRIORITY, new.STATUS, new.CATEGORY
, convert(date,new.CREATED) transaction_date --creation of ticket
, new.owner
, new.SUBMITTER
, new.company
, new.DEPARTMENT
, new.title
,'NEW' as Ticket_Type 
, convert(date,getdate()) current_date_stamp
, new.report_date
, datepart(wk,convert(date,getdate()))-11 start_week_num
, datepart(wk,convert(date,convert(date,new.CREATED))) ticket_week_num
, datepart(wk,convert(date,getdate())) current_week_num
, datediff(DAY,convert(Date,new.created),convert(date,getdate())) ticket_age
from iT_IT_CAMNewTickets new
where 1=1 and new.TIME_CLOSED is null
union
select opent.TICKET_ID, opent.type,opent.PRIORITY, opent.STATUS, opent.CATEGORY
, convert(date,opent.created)  --creation of ticket
, opent.owner
, opent.SUBMITTER
, opent.company
, opent.DEPARTMENT
, opent.title
,'OPEN' 
, convert(date,getdate())
, opent.report_date
, datepart(wk,convert(date,getdate()))-11
, datepart(wk,convert(date,convert(date,opent.CREATED)))
, datepart(wk,convert(date,getdate()))
, datediff(DAY,convert(Date,opent.created),convert(date,getdate()))
from iT_IT_CAMOpenTickets opent
where 1=1 and opent.time_closed is null
union
select closed.TICKET_ID, closed.type,closed.PRIORITY, closed.STATUS, closed.CATEGORY
, convert(date,closed.time_closed)  --closed_date
, closed.owner
, closed.SUBMITTER
, closed.company
, closed.DEPARTMENT
, closed.title
,'CLOSED'
, convert(date,getdate()) 
, closed.REPORT_DATE
, datepart(wk,convert(date,getdate()))-11 
, datepart(wk,convert(date,convert(date,closed.CREATED)))
, datepart(wk,convert(date,getdate()))
, datediff(DAY,convert(Date,closed.created),convert(date,getdate()))
from iT_IT_CAMClosedTickets closed
where closed.status='Closed' --and closed.TICKET_ID=3501
) ticket_queue
join (select max_date.ticket_id
, max(convert(date,max_date.report_date)) report_date 
from (select distinct new.ticket_id, new.Type,new.PRIORITY,new.CATEGORY
, new.owner
, new.SUBMITTER
, new.company
, new.DEPARTMENT
, new.title
, max(new.report_date) report_date
from iT_IT_CAMNewTickets new
where 1=1 and new.TIME_CLOSED is null
group by new.ticket_id, new.Type,new.PRIORITY, new.CATEGORY
, new.owner
, new.SUBMITTER
, new.company
, new.DEPARTMENT
, new.title
union
select opent.TICKET_ID, opent.type,opent.PRIORITY, opent.CATEGORY
, opent.owner
, opent.SUBMITTER
, opent.company
, opent.DEPARTMENT
, opent.title
, max(opent.report_date) 
from iT_IT_CAMOpenTickets opent
where 1=1 and opent.time_closed is null
and ticket_id=7093
group by opent.TICKET_ID, opent.type,opent.PRIORITY,  opent.CATEGORY
, opent.owner
, opent.SUBMITTER
, opent.company
, opent.DEPARTMENT
, opent.title
union
select closed.TICKET_ID, closed.type,closed.PRIORITY, closed.CATEGORY
, closed.owner
, closed.SUBMITTER
, closed.company
, closed.DEPARTMENT
, closed.title
,max(closed.REPORT_DATE) 
from iT_IT_CAMClosedTickets closed
where closed.status='Closed' 
group by closed.TICKET_ID, closed.type,closed.PRIORITY, closed.CATEGORY
, closed.owner
, closed.SUBMITTER
, closed.company
, closed.DEPARTMENT
, closed.title) max_date
where 1=1
--and max_date.TICKET_ID=6557
group by max_date.ticket_id) tx_lookup on
	ticket_queue.ticket_id=tx_lookup.TICKET_ID
	and ticket_queue.report_date=tx_lookup.report_date
, (select sub_bucket.this_week 
	, dateadd(day,-7,sub_bucket.this_week) as wk1 --dateadd(day,-7,bucket.this_week)
	, dateadd(day,-14,sub_bucket.this_week) as wk2
	, dateadd(day,-21,sub_bucket.this_week) as wk3
	, dateadd(day,-28,sub_bucket.this_week) wk4
	, dateadd(day,-35,sub_bucket.this_week) wk5
	, dateadd(day,-42,sub_bucket.this_week) wk6
	, dateadd(day,-49,sub_bucket.this_week) wk7
	, dateadd(day,-56,sub_bucket.this_week) wk8
	, dateadd(day,-63,sub_bucket.this_week) wk9
	, dateadd(day,-70,sub_bucket.this_week) wk10
	, dateadd(day,-77,sub_bucket.this_week) wk11
	, dateadd(day,-84,sub_bucket.this_week) wk12
	From (select case
	when datename(weekday,convert(date,getdate()))='Sunday' then convert(date,getdate()+6)
	when datename(weekday,convert(date,getdate()))='Monday' then convert(date,getdate()+5)
	when datename(weekday,convert(date,getdate()))='Tuesday' then convert(date,getdate()+4)
	when datename(weekday,convert(date,getdate()))='Wednesday' then convert(date,getdate()+3)
	when datename(weekday,convert(date,getdate()))='Thursday' then convert(date,getdate()+2)
	when datename(weekday,convert(date,getdate()))='Friday' then convert(date,getdate()+1)
	when datename(weekday,convert(date,getdate()))='Saturday' then convert(date,getdate())
	else ''
	end this_week) sub_bucket) bucket
	where 1=1 
	--and ticket_queue.ticket_id=6557
	group by ticket_queue.ticket_id
	, ticket_queue.type
	, ticket_queue.PRIORITY
	--, ticket_queue.STATUS
	, ticket_queue.CATEGORY
	, ticket_queue.transaction_date
	, ticket_queue.owner
	, ticket_queue.SUBMITTER
	, ticket_queue.company
	, ticket_queue.DEPARTMENT
	, ticket_queue.title
	, ticket_queue.Ticket_Type
	, ticket_queue.current_date_stamp
	, ticket_queue.start_week_num
	, ticket_queue.ticket_week_num
	, ticket_queue.current_week_num
	, ticket_queue.ticket_age
	, case	
	when convert(date,transaction_date)>=bucket.this_week then 'Not applicable'
	when convert(date,transaction_date)<=bucket.wk1 and convert(date,transaction_date)>bucket.wk2 then wk1
	when convert(date,transaction_date)<=bucket.wk2 and convert(date,transaction_date)>bucket.wk3 then wk2
	when convert(date,transaction_date)<=bucket.wk3 and convert(date,transaction_date)>bucket.wk4 then wk3
	when convert(date,transaction_date)<=bucket.wk4 and convert(date,transaction_date)>bucket.wk5 then wk4
	when convert(date,transaction_date)<=bucket.wk5 and convert(date,transaction_date)>bucket.wk6 then wk5
	when convert(date,transaction_date)<=bucket.wk6 and convert(date,transaction_date)>bucket.wk7 then wk6
	when convert(date,transaction_date)<=bucket.wk7 and convert(date,transaction_date)>bucket.wk8 then wk7
	when convert(date,transaction_date)<=bucket.wk8 and convert(date,transaction_date)>bucket.wk9 then wk8
	when convert(date,transaction_date)<=bucket.wk9 and convert(date,transaction_date)>bucket.wk10 then wk9
	when convert(date,transaction_date)<=bucket.wk10 and convert(date,transaction_date)>bucket.wk11 then wk10
	when convert(date,transaction_date)<=bucket.wk11 and  convert(date,transaction_date)>bucket.wk12 then wk11
	when convert(date,transaction_date)<=bucket.wk12 and  convert(date,transaction_date)>bucket.this_week then wk12
	end 
	, case		
		when ticket_age<7 then '<7 Days'
		when ticket_age<=14 and ticket_age>=7 then '7-14 days'
		when ticket_age<=28 and ticket_age>14 then '15-28 days'
		--when ticket_age<28 and ticket_age>=21 then '21-27 days'
		when ticket_age>28 then '>28 days'
		else ''
		end) tx
left outer join (select ticket_id
,count(ticket_id) duplicate_count
,'DUPLICATE' repeat_tix
From (select distinct ticket_queue.ticket_id
	, ticket_queue.type
	, ticket_queue.PRIORITY
	--, ticket_queue.STATUS
	, ticket_queue.CATEGORY
	, ticket_queue.transaction_date
	, ticket_queue.owner
	, ticket_queue.SUBMITTER
	, ticket_queue.company
	, ticket_queue.DEPARTMENT
	, ticket_queue.title
	, ticket_queue.Ticket_Type
	, ticket_queue.current_date_stamp
	, ticket_queue.start_week_num
	, ticket_queue.ticket_week_num
	, ticket_queue.current_week_num
	, ticket_queue.ticket_age
	, max(ticket_queue.REPORT_DATE) report_date
, case	
	when convert(date,transaction_date)>=bucket.this_week then 'Not applicable'
	when convert(date,transaction_date)<=bucket.wk1 and convert(date,transaction_date)>bucket.wk2 then wk1
	when convert(date,transaction_date)<=bucket.wk2 and convert(date,transaction_date)>bucket.wk3 then wk2
	when convert(date,transaction_date)<=bucket.wk3 and convert(date,transaction_date)>bucket.wk4 then wk3
	when convert(date,transaction_date)<=bucket.wk4 and convert(date,transaction_date)>bucket.wk5 then wk4
	when convert(date,transaction_date)<=bucket.wk5 and convert(date,transaction_date)>bucket.wk6 then wk5
	when convert(date,transaction_date)<=bucket.wk6 and convert(date,transaction_date)>bucket.wk7 then wk6
	when convert(date,transaction_date)<=bucket.wk7 and convert(date,transaction_date)>bucket.wk8 then wk7
	when convert(date,transaction_date)<=bucket.wk8 and convert(date,transaction_date)>bucket.wk9 then wk8
	when convert(date,transaction_date)<=bucket.wk9 and convert(date,transaction_date)>bucket.wk10 then wk9
	when convert(date,transaction_date)<=bucket.wk10 and convert(date,transaction_date)>bucket.wk11 then wk10
	when convert(date,transaction_date)<=bucket.wk11 and  convert(date,transaction_date)>bucket.wk12 then wk11
	when convert(date,transaction_date)<=bucket.wk12 and  convert(date,transaction_date)>bucket.this_week then wk12
	end bucket
	, case		
		when ticket_age<7 then '<7 Days'
		when ticket_age<=14 and ticket_age>=7 then '7-14 days'
		when ticket_age<=28 and ticket_age>14 then '15-28 days'
		--when ticket_age<28 and ticket_age>=21 then '21-27 days'
		when ticket_age>28 then '>28 days'
		else ''
		end Aging
FROM (select distinct new.ticket_id, new.Type,new.PRIORITY, new.STATUS, new.CATEGORY
, convert(date,new.CREATED) transaction_date --creation of ticket
, new.owner
, new.SUBMITTER
, new.company
, new.DEPARTMENT
, new.title
,'NEW' as Ticket_Type 
, convert(date,getdate()) current_date_stamp
, new.report_date
, datepart(wk,convert(date,getdate()))-11 start_week_num
, datepart(wk,convert(date,convert(date,new.CREATED))) ticket_week_num
, datepart(wk,convert(date,getdate())) current_week_num
, datediff(DAY,convert(Date,new.created),convert(date,getdate())) ticket_age
from iT_IT_CAMNewTickets new
where 1=1 and new.TIME_CLOSED is null
union
select opent.TICKET_ID, opent.type,opent.PRIORITY, opent.STATUS, opent.CATEGORY
, convert(date,opent.created)  --creation of ticket
, opent.owner
, opent.SUBMITTER
, opent.company
, opent.DEPARTMENT
, opent.title
,'OPEN' 
, convert(date,getdate())
, opent.report_date
, datepart(wk,convert(date,getdate()))-11
, datepart(wk,convert(date,convert(date,opent.CREATED)))
, datepart(wk,convert(date,getdate()))
, datediff(DAY,convert(Date,opent.created),convert(date,getdate()))
from iT_IT_CAMOpenTickets opent
where 1=1 and opent.time_closed is null
--and opent.ticket_id=3501
union
select closed.TICKET_ID, closed.type,closed.PRIORITY, closed.STATUS, closed.CATEGORY
, convert(date,closed.time_closed)  --closed_date
, closed.owner
, closed.SUBMITTER
, closed.company
, closed.DEPARTMENT
, closed.title
,'CLOSED'
, convert(date,getdate()) 
, closed.REPORT_DATE
, datepart(wk,convert(date,getdate()))-11 
, datepart(wk,convert(date,convert(date,closed.CREATED)))
, datepart(wk,convert(date,getdate()))
, datediff(DAY,convert(Date,closed.created),convert(date,getdate()))
from iT_IT_CAMClosedTickets closed
where closed.status='Closed'
) ticket_queue
join (select max_date.ticket_id
, max(convert(date,max_date.report_date)) report_date 
from (select distinct new.ticket_id, new.Type,new.PRIORITY,new.CATEGORY
, new.owner
, new.SUBMITTER
, new.company
, new.DEPARTMENT
, new.title
, max(new.report_date) report_date
from iT_IT_CAMNewTickets new
where 1=1 and new.TIME_CLOSED is null
group by new.ticket_id, new.Type,new.PRIORITY, new.CATEGORY
, new.owner
, new.SUBMITTER
, new.company
, new.DEPARTMENT
, new.title
union
select opent.TICKET_ID, opent.type,opent.PRIORITY, opent.CATEGORY
, opent.owner
, opent.SUBMITTER
, opent.company
, opent.DEPARTMENT
, opent.title
, max(opent.report_date) 
from iT_IT_CAMOpenTickets opent
where 1=1 and opent.time_closed is null
group by opent.TICKET_ID, opent.type,opent.PRIORITY,  opent.CATEGORY
, opent.owner
, opent.SUBMITTER
, opent.company
, opent.DEPARTMENT
, opent.title
union
select closed.TICKET_ID, closed.type,closed.PRIORITY, closed.CATEGORY
, closed.owner
, closed.SUBMITTER
, closed.company
, closed.DEPARTMENT
, closed.title
,max(closed.REPORT_DATE) 
from iT_IT_CAMClosedTickets closed
where closed.status='Closed' 
group by closed.TICKET_ID, closed.type,closed.PRIORITY, closed.CATEGORY
, closed.owner
, closed.SUBMITTER
, closed.company
, closed.DEPARTMENT
, closed.title
) max_date
where 1=1
--and max_date.TICKET_ID=7093
group by max_date.ticket_id) tx_lookup on
	ticket_queue.ticket_id=tx_lookup.TICKET_ID
	and ticket_queue.report_date=tx_lookup.report_date
, (select sub_bucket.this_week 
	, dateadd(day,-7,sub_bucket.this_week) as wk1 --dateadd(day,-7,bucket.this_week)
	, dateadd(day,-14,sub_bucket.this_week) as wk2
	, dateadd(day,-21,sub_bucket.this_week) as wk3
	, dateadd(day,-28,sub_bucket.this_week) wk4
	, dateadd(day,-35,sub_bucket.this_week) wk5
	, dateadd(day,-42,sub_bucket.this_week) wk6
	, dateadd(day,-49,sub_bucket.this_week) wk7
	, dateadd(day,-56,sub_bucket.this_week) wk8
	, dateadd(day,-63,sub_bucket.this_week) wk9
	, dateadd(day,-70,sub_bucket.this_week) wk10
	, dateadd(day,-77,sub_bucket.this_week) wk11
	, dateadd(day,-84,sub_bucket.this_week) wk12
	From (select case
	when datename(weekday,convert(date,getdate()))='Sunday' then convert(date,getdate()+6)
	when datename(weekday,convert(date,getdate()))='Monday' then convert(date,getdate()+5)
	when datename(weekday,convert(date,getdate()))='Tuesday' then convert(date,getdate()+4)
	when datename(weekday,convert(date,getdate()))='Wednesday' then convert(date,getdate()+3)
	when datename(weekday,convert(date,getdate()))='Thursday' then convert(date,getdate()+2)
	when datename(weekday,convert(date,getdate()))='Friday' then convert(date,getdate()+1)
	when datename(weekday,convert(date,getdate()))='Saturday' then convert(date,getdate())
	else ''
	end this_week) sub_bucket) bucket
	where 1=1 
	--and ticket_queue.ticket_id=6557
	group by ticket_queue.ticket_id
	, ticket_queue.type
	, ticket_queue.PRIORITY
	--, ticket_queue.STATUS
	, ticket_queue.CATEGORY
	, ticket_queue.transaction_date
	, ticket_queue.owner
	, ticket_queue.SUBMITTER
	, ticket_queue.company
	, ticket_queue.DEPARTMENT
	, ticket_queue.title
	, ticket_queue.Ticket_Type
	, ticket_queue.current_date_stamp
	, ticket_queue.start_week_num
	, ticket_queue.ticket_week_num
	, ticket_queue.current_week_num
	, ticket_queue.ticket_age
	, case	
	when convert(date,transaction_date)>=bucket.this_week then 'Not applicable'
	when convert(date,transaction_date)<=bucket.wk1 and convert(date,transaction_date)>bucket.wk2 then wk1
	when convert(date,transaction_date)<=bucket.wk2 and convert(date,transaction_date)>bucket.wk3 then wk2
	when convert(date,transaction_date)<=bucket.wk3 and convert(date,transaction_date)>bucket.wk4 then wk3
	when convert(date,transaction_date)<=bucket.wk4 and convert(date,transaction_date)>bucket.wk5 then wk4
	when convert(date,transaction_date)<=bucket.wk5 and convert(date,transaction_date)>bucket.wk6 then wk5
	when convert(date,transaction_date)<=bucket.wk6 and convert(date,transaction_date)>bucket.wk7 then wk6
	when convert(date,transaction_date)<=bucket.wk7 and convert(date,transaction_date)>bucket.wk8 then wk7
	when convert(date,transaction_date)<=bucket.wk8 and convert(date,transaction_date)>bucket.wk9 then wk8
	when convert(date,transaction_date)<=bucket.wk9 and convert(date,transaction_date)>bucket.wk10 then wk9
	when convert(date,transaction_date)<=bucket.wk10 and convert(date,transaction_date)>bucket.wk11 then wk10
	when convert(date,transaction_date)<=bucket.wk11 and  convert(date,transaction_date)>bucket.wk12 then wk11
	when convert(date,transaction_date)<=bucket.wk12 and  convert(date,transaction_date)>bucket.this_week then wk12
	end 
	, case		
		when ticket_age<7 then '<7 Days'
		when ticket_age<=14 and ticket_age>=7 then '7-14 days'
		when ticket_age<=28 and ticket_age>14 then '15-28 days'
		--when ticket_age<28 and ticket_age>=21 then '21-27 days'
		when ticket_age>28 then '>28 days'
		else ''
		end) base
		where 1=1
		--and ticket_id=7093
group by ticket_id
having count(ticket_id)>1) dups on
tx.ticket_id=dups.ticket_id
left outer join (select DISTINCT ticket_id, CREATED as creation_date
				From (select distinct new_sub.ticket_id
				,new_sub.created
				,min(convert(date,report_date)) report_date
				,new_sub.STATUS
				from iT_IT_CAMNewTickets  new_sub
				where 1=1 
				--and new_sub.report_date='2018-05-28'
				group by new_sub.ticket_id,new_sub.created, new_sub.status
				--brad added
				union
				select open_sub.ticket_id
				,open_sub.created
				,min(convert(date,report_date)) report_date
				,open_sub.STATUS
				from iT_IT_CAMOpenTickets  open_sub
				where 1=1 
				--and open_sub.report_date='2018-05-28'
				group by open_sub.ticket_id,open_sub.created, open_sub.status
				--end of addition
				) just_new
		where 1=1 
		--and TICKET_ID=7093
		--and status<>'Closed'
		) new on
tx.ticket_id=new.TICKET_ID
where 1=1) ct
right outer join (select ticket_id,type, PRIORITY, CATEGORY, convert(date,getdate()) transaction_date,owner,submitter, COMPANY, DEPARTMENT,title,ticket_type,convert(date,getdate()) current_date_stamp
		, convert(Date,getdate()) creation_date, 0 start_week_num, 0 ticket_week_num, 0 current_week_num, 0 ticket_age, NULL bucket, '' aging 
		From tV_IT_CAMTicketsHistory 
		where ticket_type='OPEN'
		and datediff(DAYOFYEAR,dateadd(DAYOFYEAR,-7,convert(date,REPORT_DATE)),convert(date,getdate()))<=8) hist on 
		ct.TICKET_ID=hist.ticket_id
where 1=1
--and (ct.ticket_type='OPEN' or ct.ticket_type is null)
and hist.ticket_id='7093'


--union
--select ticket_id,type, PRIORITY, CATEGORY, convert(date,getdate()) transaction_date,owner,submitter, COMPANY, DEPARTMENT,title,ticket_type,convert(date,getdate()) current_date_stamp
--, convert(Date,getdate()) creation_date, 0 start_week_num, 0 ticket_week_num, 0 current_week_num, 0 ticket_age, NULL bucket, '' aging 
--From tV_IT_CAMTicketsHistory 
--where ticket_type='OPEN'
--and datediff(DAYOFYEAR,dateadd(DAYOFYEAR,-7,convert(date,REPORT_DATE)),convert(date,getdate()))<=8

--4784
--5418
--5506
--7093