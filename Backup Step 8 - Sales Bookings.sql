truncate table iT_Sales_SalesBookings;
insert into iT_Sales_SalesBookings
select 
	 bookings.division
	, bookings.item_id
	, bookings.item_num
	, bookings.bookdate
	, bookings.promisedate
	, bookings.shipFY
	, bookings.shipPeriod
	, bookings.bqty Qty
	, bookings.bamt Amount
	, bookings.bdoco
	, bookings.blnid
	, (sum(std_cost.sduncs)*sum(bookings.bqty)) stdCost
	from
	(select avg(isnull(sd1.sduncs,sd2.sduncs)/1000000) sduncs --unit cost amount
	, ltrim(isnull(sd1.sdkcoo,sd2.sdkcoo)) sdkcoo --order company order num
	, isnull(sd1.sddoco,sd2.sddoco) order_num --order num
	, isnull(sd1.sddcto,sd2.sddcto) order_type --order type
	, isnull(sd1.sdlnid,sd2.sdlnid) line_num --line num
				from [PRODDTA].[Cam404].[PRODDTA].[f4211] sd1 
				full outer join [PRODDTA].[Cam404].[PRODDTA].[f42119] sd2 on --hist table
				ltrim(sd1.sdkcoo)=ltrim(sd2.sdkcoo)
				and sd1.sddoco=sd2.sddoco 
				and sd1.sddcto=sd2.sddcto
				and sd1.sdlnid=sd2.sdlnid
				group by ltrim(isnull(sd1.sdkcoo,sd2.sdkcoo)) 
				, isnull(sd1.sddoco,sd2.sddoco)
				, isnull(sd1.sddcto,sd2.sddcto)
				, isnull(sd1.sdlnid,sd2.sdlnid)
            ) std_cost
join (select  case
			when b.bco ='00730' then '730000'
			when b.bco='00750' then '750000'
			when b.bco='00710' then '710000'
			when b.bco='00720' then '720000'
			end Division
			, b.bco
		, b.bitm item_id
		, ltrim(b.blitm) as item_num 
		, convert(date,convert(char(10),dateadd(day,convert(int,right(b.bupmj,3)),'01/01/'+left(right(b.bupmj,5),2))-1,101)) as bookdate
		, convert(date,convert(char(10),dateadd(day,convert(int,right(b.bpddj,3)),'01/01/'+left(right(b.bpddj,5),2))-1,101)) as promisedate
		, c.fiscalYear as fYear
		, c.fiscalMonth as fMonth
        , sp.fiscalyear as shipFY
		, sp.fiscalmonth as shipFP
		,(sp.fiscalyear*100)+sp.fiscalmonth as shipPeriod
        ,sum(b.bqty) as bqty
        , sum(b.bamt/100) as bamt
		, sum(b.bdoco) as bdoco
		, sum(b.blnid) as blnid
		, b.bdcto
      from        [PRODDTA].[Cam404].[zpemjde].[bookings] b join Calendar_Dim c on b.bupmj = c.juliandate 
                              left join [PRODDTA].[Cam404].[zpemjde].[Calendar_Dim] sp on b.bpddj = sp.juliandate 
                              left outer join [PRODDTA].[Cam404].[PRODDTA].[f4105] co on b.bitm = co.coitm 
									and b.bmcu = co.comcu and co.coledg = '07' 
      where   1=1 
      and               b.blnty not in ('T')
     and               b.bdcto not in ('SA','ST','SQ')  
      group by    b.bco
	  , b.bmcu
	  , b.bitm
	  , b.blitm
	  , b.bdcto
	 , convert(date,convert(char(10),dateadd(day,convert(int,right(b.bupmj,3)),'01/01/'+left(right(b.bupmj,5),2))-1,101))
	,convert(date,convert(char(10),dateadd(day,convert(int,right(b.bpddj,3)),'01/01/'+left(right(b.bpddj,5),2))-1,101))
	  , c.fiscalYear
	  , c.fiscalMonth 
      , sp.fiscalyear
	, sp.fiscalmonth
	, co.councs) bookings on
		bookings.bco = std_cost.sdkcoo
		and bookings.bdoco = std_cost.order_num 
		and bookings.bdcto = std_cost.order_type
		and bookings.blnid = std_cost.line_num
		group by 
		 bookings.division
		, bookings.item_id
		, bookings.item_num
		, bookings.bookdate
		, bookings.promisedate
		, bookings.fYear
		, bookings.fMonth
		, bookings.shipFY
		, bookings.shipPeriod
		, bookings.bqty
		, bookings.bamt
		, bookings.bdoco
		, bookings.blnid;