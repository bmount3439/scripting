Use MoellerERP
go
select 
JONUM9, ISSU19, DUE, OSEQ19, OPER19, INDT19, WRKDLT
, lag(dateadd(d, WRKDLT*-1, DUE),1,DUE) over (partition by JONUM9, ISSU19 order by JONUM9, ISSU19, OSEQ19 desc) as ReqDate
from tV_OpenJobsRouterLT
where jonum9='24'
order by JONUM9, ISSU19, OSEQ19 desc;

select * From tV_OpenJobsRouterLT