﻿Use DataWarehouse
go
insert into iT_Purchasing_PurchaseOrdersInExcessOfDemand
select pdmcu DIVISION
	, pddoco PO_NUMBER
	, (pdlnid*.001) as LINE_NUMBER
	--, pditm ITEM_NUMBER
	, ltrim(pdlitm) as ITEM_NUMBER
	, pddsc1 as DESCRIPTION
	--, trim(pddsc2) as pddsc2
	--, pdan8 ADDRESS_NUMBER
	, pduorg QUANTITY
	--, pduopn UNITS_OPEN
	, (pdprrc*.000001) UNIT_COST
	, (pdaopn*.01)  EXTENDED_COST
	--, dba.juliantodate(pdtrdj) as pdtrdj
	, convert(date,convert(char(10),dateadd(day,convert(int,right(pdtrdj,3)),'01/01/'+left(right(pdtrdj,5),2))-1,101)) PO_DATE
	--, dba.juliantodate(pddrqj) as pddrqj
	, convert(date,convert(char(10),dateadd(day,convert(int,right(pddrqj,3)),'01/01/'+left(right(pddrqj,5),2))-1,101)) REQUEST_DATE
	--, dba.juliantodate(pdpddj) as pdpddj
	, convert(date,convert(char(10),dateadd(day,convert(int,right(pdpddj,3)),'01/01/'+left(right(pdpddj,5),2))-1,101)) PROM_DEL_DATE
	, sum(li.lipqoh) as ON_HAND_QTY
	, ib.ibsafe SAFETY
	--, (co.councs*.000001) as councs
	--, sum(li.lipcom) as sowo_soft_commit
	--, sum(li.lihcom) as so_hard_commit
	--, sum(li.lifcom) as future_commit
	--, sum(li.lifun1) as workorder_soft_commit
	--, sum(li.liqowo) as workorder_hard_commit
	--, sum(li.lipqoh)-sum(li.lipcom)-sum(li.lihcom)-sum(li.lifcom)-sum(li.liqowo)-sum(li.lifun1) as avail
	--, sum(li.liqwbo) as on_workorder
	--, sum(li.lipreq) as on_purchase_order
	--, coalesce( 
	--	(select sum(mfuorg) from [PRODDTA].[Cam404].[proddta].[f3460]
	--		where mfitm = ib.ibitm and mfmcu = ib.ibmcu and mftypf = 'BF' and convert(date,convert(char(10),dateadd(day,convert(int,right(mfdrqj,3)),'01/01/'+left(right(mfdrqj,5),2))-1,101))>convert(Date,getdate())) 
	--		,0) as fc --forecast numbers
	--demand
	, sum(li.lipcom)+ sum(li.lifcom)+sum(li.lihcom)+sum(li.liqowo)+coalesce((select sum(mfuorg) from [PRODDTA].[Cam404].[proddta].[f3460]
			where mfitm = ib.ibitm and mfmcu = ib.ibmcu and mftypf = 'BF' and convert(date,convert(char(10),dateadd(day,convert(int,right(mfdrqj,3)),'01/01/'+left(right(mfdrqj,5),2))-1,101))>convert(Date,getdate())) 
			,0) demand
	--supply
	, (sum(li.lipreq)+sum(li.liqwbo)) supply
	--excess
	, sum(li.lipqoh)-sum(li.lipcom)-sum(li.lifcom)-sum(li.lihcom)-sum(li.liqowo)+sum(li.lipreq)+sum(li.liqwbo)-coalesce((select sum(mfuorg) from [PRODDTA].[Cam404].[proddta].[f3460]
			where mfitm = ib.ibitm and mfmcu = ib.ibmcu and mftypf = 'BF' and convert(date,convert(char(10),dateadd(day,convert(int,right(mfdrqj,3)),'01/01/'+left(right(mfdrqj,5),2))-1,101))>convert(Date,getdate())) 
			,0) excess
	from [PRODDTA].[Cam404].[proddta].[f4311] pd join [PRODDTA].[Cam404].[proddta].[f4102] ib on pd.pdmcu = ib.ibmcu and pd.pditm = ib.ibitm 
	join [PRODDTA].[Cam404].[proddta].[f41021] li on ib.ibmcu = li.limcu and ib.ibitm = li.liitm 
	left join [PRODDTA].[Cam404].[proddta].[f4105] co on ib.ibitm = co.coitm and ib.ibmcu = co.comcu and co.coledg = '07' 
	where 1=1 -- trim(ib.ibmcu) = '#mcu#' 
		and ib.iblnty not in ('X','O') 
		and ib.ibstkt not in ('X') 
		and left(ib.ibglpt,1) <> 'T'  --exclude tooling
		--and pdtrdj = dba.datetojulian(current date) 
		and convert(date,convert(char(10),dateadd(day,convert(int,right(pdtrdj,3)),'01/01/'+left(right(pdtrdj,5),2))-1,101))=convert(date,getdate())
		and pdnxtr <> '999' 
		and pduopn <> 0 
		and pd.pddoco='1059068'
	group by pdmcu, pddoco, pdlnid, pditm, pdlitm, pddsc1, pddsc2, pdan8, pduorg, pduopn, 
	pdaopn, pdprrc, pdtrdj, pddrqj, pdpddj, ib.ibmcu ,ib.ibitm ,ib.iblitm 
	,ib.ibsafe, councs 
	having sum(li.lipqoh)+sum(li.lipcom)+sum(li.lihcom)+sum(li.lifcom)+sum(li.liqwbo)+sum(li.lipreq)+sum(li.liqowo)+sum(li.lifun1)<>0 --s​u​m​(​l​i​.​l​i​p​q​o​h​)​+​s​u​m​(​l​i​.​l​i​p​c​o​m​)​+​s​u​m​(​l​i​.​l​i​h​c​o​m​)​+​s​u​m​(​l​i.lifcom)+sum(li.liqwbo) 
	--+sum(li.lipreq)+sum(li.liqowo)+sum(li.lifun1) <> 0 
	order by pddoco, pdlnid 